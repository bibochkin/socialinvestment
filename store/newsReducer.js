const NEWS_DATA = 'NEWS_DATA';

export function setNewsData(data) {
    return{
        type: NEWS_DATA,
        payload: data
    }
}

const initial = {
    newsData: null
}

export function newsReducer(state = initial, action) {
    switch (action.type) {
        case NEWS_DATA:
            return {...state, newsData: action.payload}
        default: return state
    }
}