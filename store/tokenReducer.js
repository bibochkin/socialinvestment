const TOKEN_DATA = 'TOKEN_DATA';

export function tokenData(data) {
    return{
        type: TOKEN_DATA,
        payload: data
    }
}

const initial = {
    tokenDecode: null
}

export function tokenReducer(state = initial, action) {
    switch (action.type) {
        case TOKEN_DATA:
            return {...state, tokenDecode: action.payload}
        default: return state
    }
}