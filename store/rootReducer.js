import {combineReducers} from "redux";
import {tokenReducer} from "./tokenReducer";
import {newsReducer} from "./newsReducer";
import {promoReducer} from "./promoReducer";

const rootReducer = combineReducers({
    tokenReducer: tokenReducer,
    newsReducer: newsReducer,
    promoReducer: promoReducer
})

export default rootReducer;