const PROMO_DATA = 'PROMO_DATA';

export function setPromoData(data) {
    return{
        type: PROMO_DATA,
        payload: data
    }
}

const initial = {
    promoData: null
}

export function promoReducer(state = initial, action) {
    switch (action.type) {
        case PROMO_DATA:
            return {...state, promoData: action.payload}
        default: return state
    }
}