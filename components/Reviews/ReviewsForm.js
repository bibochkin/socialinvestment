import React from 'react';
import {Form, Input} from "antd";

export default function ReviewForm (props) {
    return (
        <div>
            <Form
                onFinish={props.onFinish}
                layout="vertical"
            >
                {props.reviewType === "specialist" &&
                    <p className="text-xl">
                        {`Отзыв на специалиста: ${props.fullName}`}
                    </p>
                }
                <Form.Item
                    label="Ваш отзыв"
                    name="content"
                    required
                >
                    <Input.TextArea maxLength={3000} rows={10} />
                </Form.Item>
                <Form.Item
                    label="Ваше имя"
                    name="reviewerFullName"
                    required
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Ваш email (не обязательно)"
                    name="reviewerEmail"
                    rules={[{ message: 'Пожалуйста, проверьте правильность email', type: 'email' }]}
                >
                    <Input/>
                </Form.Item>
                <button className="border-2 p-2 md:w-60 hover:border-main-color hover:shadow-lg text-lg ease-in duration-200" type="submit">Отправить</button>
            </Form>
        </div>
    )
}