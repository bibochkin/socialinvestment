import Link from "next/link";
import {CgArrowLongRight} from 'react-icons/cg'
import Image from "next/image";
import React from "react";

export default function Services ({services}) {
    return (
        <section className="my-10 md:my-20">
            <h2 className="text-3xl md:text-5xl font-oswald text-second-color">Услуги</h2>

            <div className="grid grid-cols-4 gap-10">
                <div className="col-span-4 lg:col-span-2">
                    {/*<Image src={`https://backsunrise.idea-tmn.ru/api/file/download/1643686153258_Без имени-1.png`}*/}
                    {/*       alt="Услуги клиники Рассвет"*/}
                    {/*       height={800}*/}
                    {/*       width={1000}*/}
                    {/*/>*/}
                    <img
                        src={`https://backsunrise.idea-tmn.ru/api/file/download/1643686153258_Без имени-1.png`}
                        alt="Услуги клиники Рассвет"
                        className="h-60 w-full md:h-full w-auto 2xl:h-auto 2xl:max-h-600 2xl:w-full object-cover"
                    />
                </div>

                <div className="col-span-4 lg:col-span-2 flex flex-col justify-between">
                    <div className="mb-10">
                        <p className="text-md md:text-lg">
                            Клиника красоты «Рассвет» предлагает широкий спектр косметологических услуг,
                            которые проводятся профессиональными косметологами, врачами и визажистами.
                        </p>
                    </div>
                    {services.content.map((item) => {
                        return(
                            <Link href={`/servicecategory/${item.hiddenName}`} key={item.id}>
                                <a className="flex align-center justify-between border-b border-b-gray-400 mb-5 md:mb-10 cursor-pointer hover:text-main-color ease-in duration-100">
                                    <h3 className="text-xl md:text-3xl hover:text-main-color ease-in duration-100 font-oswald">{item.name}</h3>
                                    <div className="text-3xl text-gray-600 hover:text-main-color">
                                        <CgArrowLongRight/>
                                    </div>
                                </a>
                            </Link>

                        )
                    })}
                    <div className="flex flex-row justify-between">
                        <div className="text-md md:text-2xl font-bold cursor-pointer mt-4">
                            <Link href="/servicecategory/">
                                <a className="text-main-color hover:text-main-color">
                                    <span className="border-b border-main-color">Подробнее</span>
                                </a>
                            </Link>
                        </div>
                        <div className="text-md md:text-2xl font-bold cursor-pointer mt-4">
                            <a
                                className="text-second-color hover:text-main-color"
                                href="https://app.arnica.pro/booking/booking?&orgid=47124#/services"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <span className="border-b border-second-color">Записаться на приём</span>
                            </a>
                        </div>
                    </div>

                    {/*<a href="https://app.arnica.pro/booking/booking?&orgid=47124#/services" target="_blank">*/}
                    {/*    <button className="border border-gray-400 text-black p-2 md:w-60 hover:border-main-color hover:shadow-lg text-lg ease-in duration-200">*/}
                    {/*        Записаться на приём*/}
                    {/*    </button>*/}
                    {/*</a>*/}
                </div>
            </div>

            {/*<div className="items-center grid grid-cols-4">*/}
            {/*    <div className="w-full md:w-4/12 ml-auto mr-auto px-4 col-span-2">*/}
            {/*        <img*/}
            {/*            alt="demo"*/}
            {/*            className="max-w-full rounded-lg shadow-lg"*/}
            {/*            src="https://i.ibb.co/1XQdbfx/pexels-anna-shvets-5069432.jpg"*/}
            {/*        />*/}
            {/*    </div>*/}
            {/*    <div className="w-full md:w-5/12 ml-auto mr-auto px-4 mt-4  ol-span-2">*/}
            {/*        <div className="md:pr-12">*/}
            {/*            <div*/}
            {/*                className="p-1 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-main-color mt-2"*/}
            {/*            />*/}
            {/*            <h3 className="text-4xl font-semibold">*/}
            {/*                Услуги*/}
            {/*            </h3>*/}
            {/*            <p className="mt-4 text-xl leading-relaxed text-second-color font-open-sans">*/}
            {/*                Наша клиника оказывает услуги по трём основным направдениям*/}
            {/*            </p>*/}
            {/*            <ul className="list-none mt-6 text-2xl font-open-sans">*/}
            {/*                <li className="py-2">*/}
            {/*                    <Link href="/servicecategory">*/}
            {/*                        <a className="flex items-center hover:underline">*/}
            {/*                            <span*/}
            {/*                                className="font-semibold inline-block px-2 text-main-color mr-3"*/}
            {/*                            >*/}
            {/*                                <AiOutlineSwapRight />*/}
            {/*                            </span>*/}
            {/*                            <p className="text-second-color">*/}
            {/*                                Медицинские услуги*/}
            {/*                            </p>*/}
            {/*                        </a>*/}
            {/*                    </Link>*/}
            {/*                </li>*/}
            {/*                <li className="py-2">*/}
            {/*                    <Link href="/cosmetology">*/}
            {/*                        <a className="flex items-center hover:underline">*/}
            {/*                            <span*/}
            {/*                                className="font-semibold inline-block px-2 text-main-color mr-3"*/}
            {/*                            >*/}
            {/*                                <AiOutlineSwapRight />*/}
            {/*                            </span>*/}
            {/*                            <p className="text-second-color">*/}
            {/*                                Косметологические услуги*/}
            {/*                            </p>*/}
            {/*                        </a>*/}
            {/*                    </Link>*/}

            {/*                </li>*/}
            {/*                <li className="py-2">*/}
            {/*                    <Link href="/podology">*/}
            {/*                        <a className="flex items-center hover:underline">*/}
            {/*                            <span*/}
            {/*                                className="font-semibold inline-block px-2 text-main-color mr-3"*/}
            {/*                            >*/}
            {/*                                <AiOutlineSwapRight />*/}
            {/*                            </span>*/}
            {/*                            <p className="text-second-color">*/}
            {/*                                Подология*/}
            {/*                            </p>*/}
            {/*                        </a>*/}
            {/*                    </Link>*/}

            {/*                </li>*/}
            {/*            </ul>*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*</div>*/}
        </section>
    )
}
