import Link from "next/link";

export default function Training() {
    return(
        <section>
            <h3 className="text-3xl md:text-5xl font-oswald text-second-color">Обучение косметологов</h3>
            <div className="grid grid-cols-4 gap-10  mb:my-10">
                <div className="col-span-4 md:col-span-2 order-last md:order-first">
                    <div className="text-md md:text-xl">
                        <p>Мы более 20 лет работаем в сфере эстетической медицины и косметологии и решили что пора передать свой опыт и знания.</p>
                        <p>Клиника Рассвет — это прекрасный старт для карьеры косметолога. На наших курсах вы обучитесь всем принципам эстетического ухода за
                            кожей, научитесь проводить различные косметологические процедуры на настоящих клиентах.
                            После обучения вы получите сертификат специалиста и сможете устроиться на работу в косметологическую клинику.
                        </p>
                    </div>
                </div>

                <div className="col-span-4 md:col-span-2">
                    <img src="/images/Rectangle_22.png" alt=""/>
                </div>
            </div>
            <div className="text-xl md:text-2xl text-main-color font-bold cursor-pointer">
                <Link href="/education">
                    <a>
                        <span className="text-main-color border-b border-main-color">Подробнее</span>
                    </a>
                </Link>
            </div>
        </section>
    )
}
