import { Carousel } from 'antd';
import Link from "next/link";
import React from "react";


export default function Promotions({promotions}) {

    return(
        <Carousel autoplay>
            {promotions.content.map((item) => {
                return(
                    <div className="w-full h-96 bg-second-color font-normal" key={item.id}>
                        <div className="grid md:grid-cols-2 h-96">
                            <img 
                                src={item.imagePath}
                                alt={item.title}
                                className="h-48 w-full md:h-auto md:min-h-full object-cover"
                            />
                            <div className="p-3 md:p-12 h-48 flex flex-col justify-between md:h-96">
                                <div>
                                    <h1 className="text-xl font-oswald text-main-color md:mb-4 md:text-3xl lg:text-5xl">
                                        {item.title}
                                    </h1>
                                    <p className="text-md md:text-lg lg:text-3xl text-test-1">
                                        {item.preview}
                                    </p>
                                </div>
                                <div className="mb-10">
                                    <Link href={`/promotions/${item.hiddenTitle}`}>
                                        <a 
                                            className="text-md md:text-lg lg:text-2xl text-test-1 border-b-2 py-1 border-test-1
                                            hover:cursor-pointer hover:text-main-color"
                                        >
                                            Узнать подробности
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })}
        </Carousel>
    )
}

                    // <div className="mb-5 shadow " key={item.id}>
                    //     <div className="spec__card md:bg__cover flex flex-col justify-end md:block" style={{backgroundImage: `url(${item.imagePath})`}}>
                    //         <div className="col-span-4 md:col-span-2 h-96 hidden md:block">
                    //             <div className="h-full flex flex-col">
                    //                 <div className="row-span-4 flex-1 overflow-hidden flex flex-col justify-center">
                    //                     <span
                    //                         className="font-oswald font-bold text-2xl md:text-5xl text-test-1 px-2 py-4 w-full"
                    //                         style={{backgroundImage: 'url(https://i.ibb.co/HPx9xb9/pattern.jpg)'}}
                    //                     >
                    //                         {item.title}
                    //                     </span>
                    //                     <p className="break-words mt-10 md:mt-20 text-second-color md:text-2xl" style={{textShadow: '0px 0px 1px #FFF'}}>
                    //                         {item.preview}
                    //                     </p>
                    //                 </div>

                    //                 <div className="text-2xl text-main-color font-bold cursor-pointer">
                    //                     <Link href={`/promotions/${item.hiddenTitle}`}>
                    //                         <a>
                    //                             <span className="border-b border-main-color text-main-color">Подробнее</span>
                    //                         </a>
                    //                     </Link>
                    //                 </div>
                    //             </div>
                    //         </div>

                    //         <Link href={`/promotions/${item.hiddenTitle}`}>
                    //             <a>
                    //                 <div className="bg-white px-2 py-4 font-oswald md:hidden">
                    //                     <h5 className="text-bold text-xl  font-bold text-second-color">{item.title}</h5>
                    //                     <h5 className="text-bold text-md text-main-color">{item.preview}</h5>
                    //                 </div>
                    //             </a>
                    //         </Link>
                    //     </div>

                    // </div>