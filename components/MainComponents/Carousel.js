import React from 'react';
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import Slider from "react-slick";
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/css";
import "swiper/css/pagination"
import Link from "next/link";

export default function Carousel ({specialists}) {


    return (
        <section className="w-[100%] mx-auto mb-20">
            <h1 className="text-3xl md:text-5xl font-oswald text-second-color">Специалисты</h1>
            <div className="hidden md:block">
                <Swiper slidesPerView={4} spaceBetween={5} centeredSlides={true} className="mySwiper">

                    {specialists.content.map((item, index) => {
                        return (
                            <SwiperSlide key={item.id}>
                                <div>
                                    <Link href={`/specialist/${item.hiddenFullName}`}>
                                        <a>
                                            <div className="spec__card flex flex-col justify-end drop-shadow-md m-2"
                                                 style={{backgroundImage:`url(${item.imagePath})`}}
                                            >
                                                <div className="h-1/4 bg-white px-2 py-4 font-oswald">
                                                    <h5 className="text-bold text-2xl text-second-color">{item.fullName}</h5>
                                                    <h5 className="text-bold text-xl text-main-color">{item.preview}</h5>
                                                </div>

                                            </div>
                                        </a>
                                    </Link>
                                </div>
                            </SwiperSlide>
                        )
                    })}
                </Swiper>

                {/*<Slider {...settings}>*/}
                {/*    {data.map((item, index) => {*/}
                {/*        return (*/}
                {/*            <div>*/}
                {/*                <div className="h-full spec__card"*/}
                {/*                     style={{backgroundImage: `url(${item.image})`}}*/}
                {/*                >*/}
                {/*    */}
                {/*                </div>*/}
                {/*    */}
                {/*            </div>*/}
                {/*        )*/}
                {/*    })*/}
                {/*    }*/}
                {/*</Slider>*/}
            </div>
            <div className="block md:hidden">
                {specialists.content.map((item, index) => {
                    return (
                        <div key={item.id} className="my-5">
                            <Link href={`/specialist/${item.hiddenFullName}`}>
                                <a>
                                    <div className="spec__card flex flex-col justify-end drop-shadow-md"
                                         style={{backgroundImage:`url(${item.imagePath})`}}
                                    >
                                        <div className="bg-white px-2 py-4 font-oswald">
                                            <h5 className="text-bold text-xl  font-bold text-second-color">{item.fullName}</h5>
                                            <h5 className="text-bold text-md text-main-color">{item.preview}</h5>
                                        </div>

                                    </div>
                                </a>
                            </Link>
                        </div>
                    )
                })}

                <div className="text-xl md:text-2xl text-main-color font-bold cursor-pointer mt-2">
                    <Link href={`/specialist`}>
                        <a>
                            <span className="border-b border-main-color text-main-color">Подробнее</span>
                        </a>
                    </Link>
                </div>
            </div>



        </section>
    )
}


/*













*/
