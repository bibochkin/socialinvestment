import {CgArrowLongRight} from "react-icons/cg";
import Link from "next/link";
import React from "react";

export default function MainPageAbout() {
    return(
        <section className="mb-20">
            <h2 className="text-3xl md:text-5xl font-oswald text-second-color">О клинике</h2>

            <div className="grid grid-cols-4 gap-10">
                <div className="col-span-4 lg:col-span-2">
                    <img
                        src="/images/Rectangle_6.webp"
                        alt="О клинике"
                        className="h-full w-auto object-cover"
                    />
                </div>

                <div className="col-span-4 lg:col-span-2 flex flex-col justify-between">
                    <div className="mb-5 md:mb-10">
                        <h3 className="text-2xl md:text-3xl font-oswald text-second-color">Красота через здоровье</h3>

                        <p className="text-md md:text-xl">
                            Клиника красоты «Рассвет» предлагает широкий спектр косметологических услуг,
                            которые проводятся профессиональными косметологами, врачами и визажистами.
                        </p>

                        <p className="text-md md:text-xl">
                            В нашей клинике применяются современные методики, которые позволят добиться
                            самых смелых и естественных результатов в области преображения внешности.
                        </p>

                        <p className="text-md md:text-xl">
                            {`
                            Чтобы подобрать подходящую именно вам процедуру,
                            обратитесь в нашу косметологическую клинику красоты "Рассвет". Вы можете записаться
                            на процедуру онлайн. Нас выбирают те, кто ценит безопасность, эффективность и эстетику
                            `}
                        </p>
                    </div>

                    <div className="flex flex-row justify-between">
                        <div className="text-md md:text-2xl font-bold cursor-pointer mt-4">
                            <Link href="/servicecategory/">
                                <a className="text-main-color hover:text-main-color">
                                    <span className="border-b border-main-color">Подробнее</span>
                                </a>
                            </Link>
                        </div>
                        <div className="text-md md:text-2xl font-bold cursor-pointer mt-4">
                            <a
                                className="text-second-color hover:text-main-color"
                                href="https://app.arnica.pro/booking/booking?&orgid=47124#/services"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <span className="border-b border-second-color">Записаться на приём</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    )
}
