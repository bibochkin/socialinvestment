import React, {useEffect, useRef, useState} from 'react';
import {Button, Form, Input, notification, Tag, Select, InputNumber} from "antd";
import FilesPanel from "./FilesPanel";
import API from '../API/API';
import CyrillicToTranslit from "cyrillic-to-translit-js";


export default function EditRecord (props) {

    const editorRef = useRef();
    const {recordType} = props;
    const {Option} = Select;
    const [visible, isVisible] = useState(false);
    const [editorLoaded, setEditorLoaded] = useState(false);
    const [contentData, changeData] = useState(null);
    const [mainImage, addImage] = useState('');
    const [name, setName] = useState('');
    const [label, setLabel] = useState('');
    const [category, setCategory] = useState(null);
    const [drug, setDrug] = useState(null);
    const [equipment, setEquipment] = useState(null);
    const [specialist, setSpecialist] = useState(null);
    const [service, setService] = useState(null);
    const {CKEditor, ClassicEditor} = editorRef.current || {};
    const cyrillicToTranslit = new CyrillicToTranslit();

    useEffect( () => {
        editorRef.current = {
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor,
            ClassicEditor: require('@ckeditor/ckeditor5-build-classic')
        }
        setEditorLoaded(true);

        if (recordType === "specialist") {
            setName("fullName");
            setLabel("ФИО");
        } else {
            setName("name");
            setLabel("Наименование");
        }

        if (category === null) {
            API.get('api/clinic-services/category?size=999')
                .then(function(response){
                    setCategory(response.data.clinicServiceCategoryDataPage.content);
                })
                .catch(function (error){onError(error)})
            API.get('api/clinic-services/drug?size=999')
                .then(function(response){
                    setDrug(response.data.drugDataPage.content);
                })
                .catch(function(error){onError(error)})
            API.get('api/clinic-services/equipment?size=999')
                .then(function (response){
                    setEquipment(response.data.equipmentDataPage.content);
                })
                .catch(function(error){onError(error)})
            API.get('api/clinic-services/specialist?size=999')
                .then(function (response){
                    setSpecialist(response.data.specialistDataPage.content);
                })
                .catch(function(error){onError(error)})
            API.get('api/clinic-services/service?size=999')
                .then(function (response){
                    setService(response.data.clinicServiceDataPage.content);
                })
                .catch(function (error){onError(error)})
        }
    }, [] )

    function onError (error) {
        return (
            notification['error']({
                message: 'Что-то пошло не так :(',
                description: error.message
            })
        )
    }

    if (ClassicEditor) {
        ClassicEditor.defaultConfig.toolbar = {
            items: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', '|', 'uploadImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'undo', 'redo']
        }
    }

    function onFinish (values) {

        if (values.drugIdList === undefined)
        {values.drugIdList = props.initValues.drugIdList}
        if (values.equipmentIdList === undefined)
        {values.equipmentIdList = props.initValues.equipmentIdList}
        if (values.specialistIdList === undefined)
        {values.specialistIdList = props.initValues.specialistIdList}
        if (values.clinicServiceCategoryId === undefined)
        {values.clinicServiceCategoryId = props.initValues.clinicServiceCategoryId}

        const reg = /[^a-z0-9_]+/g;
        if (recordType === "specialist") {
            values.hiddenFullName = cyrillicToTranslit.transform(values.fullName, '_').toLowerCase().replace(reg, '');
        } else {
            values.hiddenName = cyrillicToTranslit.transform(values.name, '_').toLowerCase().replace(reg, '');
        }

        if (contentData) {
            values.content = contentData;
        } else if (props.initValues) {
            values.content = props.initValues.content;
        } else {
            notification['warning']({
                message: 'Не заполнено полное описание',
                className: "font-open-sans"
            })
        }

        if (mainImage) {
            values.imagePath = mainImage;
        } else if (props.initValues) {
            values.imagePath = props.initValues.imagePath;
        } else {
            notification['warning']({
                message: 'Не добавлено изображение',
                className: "font-open-sans"
            })
        }

        values.id = props.initValues.id;

        if (recordType === "category" && values.content) {
            API.put(`api/clinic-services/category`, values)
                .then(function (response) {
                    notification['success']({
                        message: response.data.message,
                        className: "font-open-sans"
                    })
                    addImage('');
                    changeData(null);
                    props.onCategorySuccess();
                })
                .catch(function(error){onError(error)})
        } else if (values.content && values.imagePath) {
            API.put(`api/clinic-services/${recordType}`, values)
                .then(function (response) {
                    notification['success']({
                        message: response.data.message,
                        className: "font-open-sans"
                    })
                    addImage('');
                    changeData(null);
                    switch (props.recordType) {
                        case "specialist":
                            return props.onTeamSuccess();
                        case "drug":
                            return props.onDrugSuccess();
                        case "equipment":
                            return props.onEquipmentSuccess();
                        case "service":
                            return props.onServiceSuccess();
                        default:
                            break;
                    }
                })
                .catch(function(error){onError(error)})
        }
    }

    function setImage (item) {
        addImage(item);
        isVisible(false)
    }

    return (
        <div className="font-open-sans bg-white overflow-hidden w-full sm:px-6 lg:px-8">
            <FilesPanel
                visible={visible}
                onCancel={()=>{isVisible(false)}}
                setImage={setImage}
            />
            {editorLoaded &&
            <Form
                name="record"
                onFinish={onFinish}
                layout="vertical"
                fields ={props.fields}
            >
                <Form.Item
                    label={label}
                    name={name}
                    rules={[{ required: true, message: `Пожалуйста, введите ${label}` }]}
                >
                    <Input
                        type="text"
                        className="border-gray-300 w-full text-xl"
                        name={name}
                        id={name}
                        required
                        maxLength={40}
                    />
                </Form.Item>

                {(recordType === "service" || recordType === "drug") &&
                <Form.Item
                    label="Цена"
                    name="price"
                    required={recordType === "service"}
                >
                    <InputNumber controls={false} />
                </Form.Item>
                }

                <div className="mb-4">
                    <Button type="primary" onClick={()=>{isVisible(true)}}>Добавить основное фото</Button>
                    <div className="mt-2">
                        Изображение выбрано:
                        <Tag color={"green"}>
                            {props.initValues?.imagePath}
                        </Tag>
                    </div>
                </div>

                {recordType === "service" &&
                <Form.Item
                    label="Категория"
                    name="clinicServiceCategoryId"
                    required={recordType === "service"}
                >
                    {category !== null &&
                    <Select defaultValue={[props.initValues.clinicServiceCategoryId]} name="clinicServiceCategoryId">
                        {category.map((obj)=><Option value={obj.id} key={obj.id}>{obj.name}</Option>)}
                    </Select>
                    }
                </Form.Item>
                }

                {(recordType === "service" || recordType === "specialist") &&
                <Form.Item
                    label="Препараты"
                    name="drugIdList"
                    required={recordType === "service"}
                >
                    {drug !== null &&
                    <Select mode="multiple"  defaultValue={props.initValues.drugIdList} name="drugIdList">
                        {drug.map((obj)=><Option value={obj.id} key={obj.id}>{obj.name}</Option>)}
                    </Select>
                    }
                </Form.Item>
                }

                {(recordType === "service" || recordType === "specialist") &&
                <Form.Item
                    label="Оборудование"
                    name="equipmentIdList"
                    required={recordType === "service"}
                >
                    {equipment !== null &&
                    <Select mode="multiple"  defaultValue={props.initValues.equipmentIdList} name="equipmentIdList">
                        {equipment.map((obj)=><Option value={obj.id} key={obj.id}>{obj.name}</Option>)}
                    </Select>
                    }
                </Form.Item>
                }

                {(recordType === "service" || recordType === "drug" || recordType === "equipment") &&
                <Form.Item
                    label="Специалисты"
                    name="specialistIdList"
                    required={recordType === "service"}
                >
                    {specialist !== null &&
                    <Select mode="multiple" defaultValue={props.initValues.specialistIdList} name="specialistIdList">
                        {specialist.map((obj)=><Option value={obj.id} key={obj.id}>{obj.fullName}</Option>)}
                    </Select>
                    }
                </Form.Item>
                }

                {(recordType === "specialist" || recordType === "drug" || recordType === "equipment") &&
                <Form.Item
                    label="Услуги"
                    name="clinicServiceIdList"
                >
                    {service &&
                    <Select mode="multiple"  defaultValue={props.initValues.clinicServiceIdList} name="clinicServiceIdList">
                        {service.map((obj)=><Option value={obj.id} key={obj.id}>{obj.name}</Option>)}
                    </Select>
                    }
                </Form.Item>
                }

                <Form.Item
                    label="Краткое описание"
                    name="preview"
                >
                    <Input
                        type="text"
                        className="border-2 border-gray-300 p-2 w-full text-xl"
                        name="preview"
                        id="preview"
                        maxLength={150}
                    />
                </Form.Item>

                <Form.Item
                    label="Полное описание"
                    name="content"
                >
                    <CKEditor
                        id="editor"
                        editor={ ClassicEditor }
                        data={props.initValues?.content}
                        onChange={(event, editor) => {
                            changeData(editor.getData());
                        }}
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="text-sm font-semibold px-14 py-3 rounded"
                    >
                        Сохранить изменения
                    </Button>
                </Form.Item>
            </Form>
            }
        </div>
    )
}