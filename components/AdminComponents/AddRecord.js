import React, {useEffect, useRef, useState} from 'react';
import {Button, Form, Input, notification, Tag} from "antd";
import FilesPanel from "./FilesPanel";
import API from '../API/API';
import CyrillicToTranslit from "cyrillic-to-translit-js";


export default function AddRecord (props) {

    const editorRef = useRef();
    const [visible, isVisible] = useState(false);
    const [editorLoaded, setEditorLoaded] = useState(false);
    const [contentData, changeData] = useState(null);
    const [mainImage, addImage] = useState('');
    const {CKEditor, ClassicEditor} = editorRef.current || {};
    const cyrillicToTranslit = new CyrillicToTranslit();

    useEffect( () => {
        editorRef.current = {
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor,
            ClassicEditor: require('@ckeditor/ckeditor5-build-classic')
        }
        setEditorLoaded(true);
    }, [] )

    if (ClassicEditor) {
        ClassicEditor.defaultConfig.toolbar = {
            items: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', '|', 'uploadImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'undo', 'redo']
        }
    }

    function onFinish (values) {

        const reg = /[^a-z0-9_]+/g;
        values.hiddenTitle = cyrillicToTranslit.transform(values.title, '_').toLowerCase().replace(reg, '');

        if (contentData) {
            values.content = contentData;
        } else {
            notification['warning']({
                message: 'Не заполнено полное описание',
                className: "font-open-sans"
            })
        }

        if (mainImage) {
            values.imagePath = mainImage;
        } else {
            notification['warning']({
                message: 'Не добавлено изображение',
                className: "font-open-sans"
            })
        }

        if (values.content && values.imagePath) {
            API.post(`api/dynamicPages/${props.recordType}`, values)
                .then(function (response) {
                    notification['success']({
                        message: response.data.message,
                        className: "font-open-sans"
                    })
                    addImage('');
                    changeData(null);
                    switch (props.recordType) {
                        case "news":
                            return props.onNewsSuccess();
                        case "promotion":
                            return props.onPromoSuccess();
                        default:
                            break;
                    }
                })
                .catch(function (error) {
                    notification['error']({
                        message: 'Что-то пошло не так :(',
                        description: error.message,
                        className: "font-open-sans"
                    })
                })
        }
    }

    function setImage (item) {
        addImage(item);
        isVisible(false)
    }

    return (
        <div className="font-open-sans bg-white overflow-hidden w-full sm:px-6 lg:px-8">
            <FilesPanel
                visible={visible}
                onCancel={()=>{isVisible(false)}}
                setImage={setImage}
            />
            {editorLoaded &&
            <Form
                name="record"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    label="Заголовок"
                    name="title"
                    rules={[{ required: true, message: 'Пожалуйста, введите заголовок' }]}
                >
                    <Input
                        type="text"
                        className="w-full text-xl"
                        name="title"
                        id="title"
                        required
                        maxLength={40}
                    />
                </Form.Item>



                <div className="mb-4">
                    <Button type="primary" onClick={()=>{isVisible(true)}}>Добавить основное фото</Button>
                    {mainImage &&
                        <div className="mt-2">
                            Изображение выбрано:
                            <Tag color={"green"}>
                                {mainImage}
                            </Tag>
                        </div>
                    }
                </div>

                <Form.Item
                    label="Краткое описание"
                    name="preview"
                >
                    <Input
                        type="text"
                        className="border-2 border-gray-300 p-2 w-full text-xl"
                        name="preview"
                        id="preview"
                        maxLength={150}
                    />
                </Form.Item>

                <Form.Item
                    label="Полное описание"
                    name="content"
                >
                    <CKEditor
                        id="editor"
                        editor={ ClassicEditor }
                        data=""
                        onChange={(event, editor) => {
                            changeData(editor.getData());
                        }}
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="text-sm font-semibold px-14 py-3 rounded"
                    >
                        Опубликовать
                    </Button>
                </Form.Item>
            </Form>
            }
        </div>
    )
}