import React, {useEffect, useState} from 'react';
import {Button, Form, Input, notification} from "antd";
import jwt_decode from "jwt-decode";
import API from "../API/API";

export default function EditAccount (props) {

    const [user, setUser] = useState(null);

    useEffect(()=>{
        const currentUser = jwt_decode(props.token);
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (!user) {
            API.get(`api/account?login=${currentUser.sub}`)
                .then(function(response){
                    const data = response.data.accountsDataPage.content[0];
                    setUser(data);
                })
                .catch(function(error){
                    notification['error']({
                        message: 'Что-то пошло не так :(',
                        description: error.message
                    })
                })
        }
    }, [])

    function onFinish (values) {
        values.accountId = user.id;
        API.put('api/account', values)
            .then(function(response){
                notification['success']({
                    message: response.data.message,
                })
                props.close();
            })
            .catch(function(error){
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message
                })
            })
    }

    return (
        <div>
            {user &&
            <Form
                layout="vertical"
                name="nest-messages"
                onFinish={onFinish}
                fields ={[
                    {name: ["login"], value: user.login},
                    {name: ["email"], value: user.email},
                    {name: ["firstName"], value: user.fullName.split(' ')[1]},
                    {name: ["lastName"], value: user.fullName.split(' ')[0]},
                ]}
            >
                <Form.Item name={'login'} label="Логин" rules={[{ required: true, message: 'Не выбран логин' }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={'email'} label="Email" rules={[{ type: 'email', required: true, message: 'Не выбран email' }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={'firstName'} label="Имя" rules={[{ required: true, message: 'Не выбрано имя' }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={'lastName'} label="Фамилия" rules={[{ required: true, message: 'Не выбрана фамилия' }]}>
                    <Input />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Сохранить изменения
                    </Button>
                </Form.Item>
            </Form>
            }
        </div>
    )
}