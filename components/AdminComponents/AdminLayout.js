import {Drawer, Layout, Menu, notification, Spin} from "antd";
import 'antd/dist/antd.css';
import {
    CommentOutlined,
    DesktopOutlined,
    ExperimentOutlined, HeartOutlined,
    PieChartOutlined, ProfileOutlined, RocketOutlined,
    SolutionOutlined,
    TeamOutlined,
    UserOutlined
} from "@ant-design/icons";
import jwt_decode from "jwt-decode";
import React, {useEffect, useState} from "react";
import API from "../API/API";
import store from "../../store/store";
import {tokenData} from "../../store/tokenReducer";
import Router from "next/router";
import EditAccount from "./EditAccount";
import moment from "moment";
import Link from "next/link";

export default function AdminLayout ({children}) {

    const {Sider} = Layout;
    const {SubMenu} = Menu;
    const {push, asPath} = Router;

    const [collapsed, onCollapse] = useState(false);
    const [token, setToken] = useState(null);
    const [visible, isVisible] = useState(false);
    const [loading, isLoading] = useState(false);

    useEffect(()=>{
        Router.events.on('routeChangeStart', ()=>{isLoading(true)})
        if (localStorage['token']) {
            if (moment().valueOf() > 1000*jwt_decode(localStorage['token']).exp) {               // - 86395000
                localStorage.removeItem('token');
                push('/admin');
            } else {
                API.defaults.headers.common['Authorization'] = localStorage['token'];
                setToken(localStorage['token']);
            }
        }
    }, [])

    function logOut () {
        let body = { token: localStorage.getItem('token') };

        API.post('api/auth/logout', body)
            .then(function (response) {
                localStorage.removeItem('token');
                store.dispatch(tokenData(null));
                setToken(null);
                push('/admin');
                notification['success']({
                    message: response.data.message,
                    className: "font-open-sans"
                })
            })
            .catch(function (error) {
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    function selectedKeys () {
        switch (asPath) {
            case "/admin/news":
                return ["news"]
            case "/admin/team":
                return ["team"]
            case "/admin/accounts":
                return ["accounts"]
            case "/admin/categories":
                return ["categories"]
            case "/admin/services":
                return ["services"]
            case "/admin/reviews":
                return ["reviews"]
            default:
                return ["news"]
        }
    }

    return (
        <div className="font-open-sans">
            <Drawer
                placement="left"
                onClose={()=>{
                    isVisible(false);
                }}
                visible={visible}
                width="40%"
            >
                <EditAccount token={token} close={()=>{isVisible(false)}}/>
            </Drawer>

            <Layout style={{ minHeight: '100vh' }}>
                <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} >
                    <div className="logo" />
                    <Menu theme="dark" defaultSelectedKeys={selectedKeys()} mode="inline">
                        <Menu.Item key="news" icon={<DesktopOutlined />} >
                            <Link href={"/admin/news"}>Новости</Link>
                        </Menu.Item>
                        <Menu.Item key="team" icon={<SolutionOutlined />} >
                            <Link href={"/admin/team"}>Специалисты</Link>
                        </Menu.Item>
                        <Menu.Item key="services" icon={<HeartOutlined />} >
                            <Link href={"/admin/services"}>Услуги</Link>
                        </Menu.Item>
                        <Menu.Item key="categories" icon={<ProfileOutlined />} >
                            <Link href={"/admin/categories"}>Категории</Link>
                        </Menu.Item>
                        <Menu.Item key="reviews" icon={<CommentOutlined />} >
                            <Link href={"/admin/reviews"}>Отзывы</Link>
                        </Menu.Item>
                        <Menu.Item key="accounts" icon={<TeamOutlined />} >
                            <Link href={"/admin/accounts"}>Пользователи</Link>
                        </Menu.Item>
                        <SubMenu key="sub1" icon={<UserOutlined />} title="Профиль" >
                            <Menu.Item key="edit" onClick={()=>{isVisible(true)}}>Редактировать</Menu.Item>
                            <Menu.Item key="logout" onClick={logOut} >Выйти</Menu.Item>
                        </SubMenu>
                    </Menu>
                </Sider>

                {loading &&
                <div className="min-h-screen w-full mx-6 text-center flex flex-col justify-center" >
                    <Spin tip="Загрузка..." size="large"/>
                </div>
                }
                {!loading &&
                <div className="min-h-screen w-full mx-6" >
                    {children}
                </div>
                }

            </Layout>
        </div>
    )
}