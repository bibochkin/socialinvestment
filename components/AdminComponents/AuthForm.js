import {Form, Button, Input, notification, Result} from "antd";
import React, {useState} from "react";
import API from "../API/API";

export default function AuthForm (props) {

    const [mode, changeMode] = useState('login');
    const [loading, setLoading] = useState(false);

    function onFinishPassword (values) {
        setLoading(true);
        let payload = null;
        if (values.login) {
            payload = {login: values.login}
        } else if (values.email) {
            payload = {email: values.email}
        } else {
            notification['error']({
                message: 'Введите логин или пароль'
            })
        }
        if (payload) {
            API.post('api/auth/reset-password', payload)
                .then(function(response){
                    notification['success']({
                        message: response.data.message
                    })
                    changeMode('waiting')
                    setLoading(false)
                })
                .catch(function(error){
                    notification['error']({
                        message: 'Что-то пошло не так :(',
                        description: error.message,
                        className: "font-open-sans"
                    })
                    setLoading(false)
                })
        }
    }

    return (
        <div>
            {mode==='login' &&
            <Form
                name="basic"
                labelCol={{ span: 5 }}
                wrapperCol={{ span: 17 }}
                onFinish={props.onFinish}
                onFinishFailed={props.onFinishFailed}
                autoComplete="off"
                className="bg-white"
            >
                <h1 className="text-2xl font-oswald text-center mb-6">АВТОРИЗАЦИЯ</h1>
                <Form.Item
                    label="Логин"
                    name="login"
                    rules={[{ required: true, message: 'Пожалуйста, введите логин' }]}
                    className=""
                >
                    <Input className="" />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{ required: true, message: 'Пожалуйста, введите пароль' }]}
                    className=""
                >
                    <Input.Password className="" />
                </Form.Item>

                <Form.Item
                    wrapperCol={{ offset: 9, span: 17 }}
                >
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="text-sm font-semibold px-14 py-3 rounded"
                        loading={props.loading}
                    >
                        Войти в аккаунт
                    </Button>
                </Form.Item>
                <div className="h-10 text-center text-blue-400">
                    <span className="hover:cursor-pointer hover:text-main-color" onClick={()=>{changeMode('password')}}>
                        Забыли пароль?
                    </span>
                </div>
            </Form>
            }
            {mode==='password' &&
            <Form
                name="recovery"
                labelCol={{ span: 5 }}
                wrapperCol={{ span: 17 }}
                onFinish={onFinishPassword}
                autoComplete="off"
                className="bg-white"
            >
                <h1 className="text-2xl font-oswald text-center mb-3">Восстановление пароля</h1>
                <p className="text-center">Введите ваш логин или адрес электронной почты</p>
                <Form.Item label="Логин" name="login">
                    <Input />
                </Form.Item>

                <Form.Item label="Email" name="email">
                    <Input />
                </Form.Item>

                <Form.Item
                    wrapperCol={{ offset: 9, span: 17 }}
                >
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="text-sm font-semibold px-14 py-3 rounded"
                        loading={loading}
                    >
                        Восстановить
                    </Button>
                </Form.Item>
                <p className="text-center pb-3">На вашу электронную почту придёт письмо со ссылкой для сброса пароля</p>
            </Form>
            }
            {mode === 'waiting' &&
            <div>
                <Result
                    title="Проверьте почту!"
                />
            </div>
            }
        </div>

    )

}