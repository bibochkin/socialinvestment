import React, {useState} from "react";
import {Button, Layout, Menu} from 'antd';
import {
    DesktopOutlined,
    PieChartOutlined,
    UserOutlined, TeamOutlined, SolutionOutlined,
} from '@ant-design/icons';
import 'antd/dist/antd.css';
import jwt_decode from "jwt-decode";
import News from "../../pages/admin/news";
import Promotions from "../../pages/admin/promotions";
import Team from "../../pages/admin/team";
import Accounts from "../../pages/admin/accounts";

export default function AdminPanel (props) {

    const {Content, Sider} = Layout;
    const {SubMenu} = Menu;

    const [collapsed, onCollapse] = useState(false);

    const [content, changeContent] = useState('promotions');

    return (
        <div className="font-open-sans">
            {/*<Layout style={{ minHeight: '100vh' }}>*/}
            {/*    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} >*/}
            {/*        <div className="logo" />*/}
            {/*        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">*/}
            {/*            <Menu.Item key="1" icon={<PieChartOutlined />} onClick={()=>{changeContent('promotions')}} >*/}
            {/*                Акции*/}
            {/*            </Menu.Item>*/}
            {/*            <Menu.Item key="2" icon={<DesktopOutlined />} onClick={()=>{changeContent('news')}} >*/}
            {/*                Новости*/}
            {/*            </Menu.Item>*/}
            {/*            <Menu.Item key="6" icon={<SolutionOutlined />} onClick={()=>{changeContent('team')}}>*/}
            {/*                Специалисты*/}
            {/*            </Menu.Item>*/}
            {/*            <Menu.Item key="10" icon={<TeamOutlined />} onClick={()=>{changeContent('accounts')}}>*/}
            {/*                Пользователи*/}
            {/*            </Menu.Item>*/}
            {/*            <SubMenu key="sub1" icon={<UserOutlined />} title="Профиль">*/}
            {/*                <div className="my-6 text-center">{jwt_decode(props.token).username}</div>*/}
            {/*                <Button type="danger" onClick={props.logOut} style={{ width: '100%' }}>Выйти</Button>*/}
            {/*            </SubMenu>*/}
            {/*        </Menu>*/}
            {/*    </Sider>*/}
            {/*    <Content style={{ margin: '0 16px', height: 'auto' }} >*/}
                    {content === 'news' && <News />}
                    {content === 'promotions' && <Promotions />}
                    {content === 'team' && <Team />}
                    {content === 'accounts' && <Accounts />}
            {/*    </Content>*/}
            {/*</Layout>*/}
        </div>
    )
}