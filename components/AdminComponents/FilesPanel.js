import React, {useEffect, useState} from "react";
import {Menu, Button, Upload, Modal, notification, PageHeader, Radio, Dropdown} from "antd";
import API from "../API/API";
import Image from "next/image";


export default function FilesPanel (props) {

    const [fileList, changeFileList] = useState(null);
    const [confirmVisible, isConfirmVisible] = useState(false);
    const [currentId, setId] = useState(null);
    const [currentSrc, setSrc] = useState(null);

    useEffect(()=>{
        if(fileList === null) {
            getAllFiles();
        }
    });

    const getAllFiles = () => {
        API.get('api/file')
            .then(function (response) {
                const data = response.data.filesList;
                changeFileList(data);
            })
            .catch(function(error) {
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    const newImage = (options) => {
        const data= new FormData()
        data.append('file', options.file)
        API.post('api/file/upload', data)
            .then(function (response)  {
                getAllFiles();
            })
            .catch(function (error) {
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    const removeImage = (uid) => {
        API.delete(`api/file/${uid}`)
            .then(function (response) {
                notification['success']({
                    message: response.data.message,
                    className: "font-open-sans"
                })
                getAllFiles();
                isConfirmVisible(false);
            })
            .catch(function (error) {
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
                isConfirmVisible(false);
            })
    }

    const downloadImage = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
        };

        fetch(currentSrc, requestOptions)
            .then(response => response.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                a.download = `${currentId}`;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            })
            .catch(error => console.log('error', error));
    }

    const uploadSettings = {
        accept: 'image/*',
        name: 'file',
        multiple: false,
        maxCount: 1,
        customRequest: (options) => {
            newImage(options)
        },
        showUploadList: false
    }

    return (
        <div className="font-open-sans">
            <Modal
                visible={props.visible}
                footer={null}
                onCancel={props.onCancel}
                width={'65%'}
                style={{height: "auto", top: 10, padding: 1}}
            >
                <PageHeader
                    className="font-open-sans"
                    title="Выберите фото"
                    style={{position: "sticky"}}
                    extra={[
                        <Button
                            key="3"
                            type="primary"
                            disabled={!currentId}
                            onClick={()=>{
                                props.setImage(currentSrc)
                            }}
                        >
                            Выбрать
                        </Button>,
                        <Button
                            key="3"
                            type="primary"
                            disabled={!currentId}
                            onClick={()=>{
                                downloadImage()
                            }}
                        >
                            Скачать
                        </Button>,
                        <Button
                            key="2"
                            type="primary"
                            disabled={!currentId}
                            onClick={()=>{
                                isConfirmVisible(true)
                            }}
                        >
                            Удалить
                        </Button>,
                        <Upload {...uploadSettings} key="4" >
                            <Button
                                key="1"
                                type="primary"
                            >
                                Добавить
                            </Button>,
                        </Upload>
                    ]}
                />
                <Modal
                    title="Внимание!"
                    visible={confirmVisible}
                    onOk={()=>{removeImage(currentId)}}
                    onCancel={()=>{isConfirmVisible(false)}}
                    okText="Удалить"
                    cancelText="Отмена"
                >
                    <p className="font-open-sans">Вы уверены, что хотите удалить это изображение?</p>
                </Modal>
                <div className="container px-5 py-2 mx-auto overflow-y-scroll" style={{height: "700px"}}>
                    <Radio.Group
                        className="flex flex-wrap flex-row justify-center"
                        onChange={(event)=>{
                            setId(event.target.value);
                            setSrc(event.target.link);
                        }}
                        defaultValue={null}
                    >
                            {fileList ? fileList.map((item)=>{
                                return (
                                        <Radio.Button
                                            key={item.id}
                                            link={item.link}
                                            value={item.id}
                                            className="hover:shadow-xl"
                                            defaultChecked={false}
                                            style={{height: "200px", width: "200px", padding: "2px", margin: "5px"}}
                                        >
                                            <div className="h-auto w-auto">
                                                <Image
                                                    alt={item.name}
                                                    id={item.id}
                                                    key={item.id}
                                                    src={item.link}
                                                    width={200}
                                                    height={200}
                                                    objectFit="cover"
                                                />
                                            </div>
                                        </Radio.Button>
                                )
                            }) : "Загрузка..."}
                    </Radio.Group>
                </div>
            </Modal>
        </div>
    )
}