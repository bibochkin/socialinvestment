import React from 'react';
import 'antd/dist/antd.css';
import {Button, Drawer, Menu, Space} from "antd";
import {CloseOutlined} from '@ant-design/icons'
import Link from "next/link";
import {useRouter} from 'next/router'

export default function MainNavigation(props) {
    const router = useRouter();
    const {SubMenu} = Menu;

    return (
        <Drawer title="Меню"
                placement="right"
                extra={
                    <Space>
                        <Button onClick={props.onClose}
                                shape="circle"
                                icon={<CloseOutlined/>}
                        />
                    </Space>
                }
                closable={false}
                onClose={props.onClose}
                visible={props.visible}
                width={'100vw'}
                bodyStyle={{padding: 20}}
        >

            <Menu mode="inline"
                  className="divide-y divide-slate-200"
                  defaultSelectedKeys={[router.asPath.split('/')[1]]}
            >
                <SubMenu key="clinics" title="Услуги">
                    <Menu.Item key="servicecategory">
                        <Link href={'/servicecategory'}>
                            <a>Все услуги</a>
                        </Link>
                    </Menu.Item>
                    {props.services?.content.map((item, index) => {
                        return (
                            <Menu.Item key={index}>
                                <Link href={`/servicecategory/${item.hiddenName}`}>
                                    <a>
                                        {item.name}
                                    </a>
                                </Link>
                            </Menu.Item>


                        )
                    })}
                </SubMenu>
                <Menu.Item key={'education'}>
                    <Link href={"/education"}>
                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('education') ? 'text-main-color' : 'text-second-color'}`}>
                            Обучение
                        </a>
                    </Link>
                </Menu.Item>
                <Menu.Item key={'specialist'}>
                    <Link href={"/specialist"}>
                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('specialist') ? 'text-main-color' : 'text-second-color'}`}>
                            Специалисты
                        </a>
                    </Link>
                </Menu.Item>
                <Menu.Item key='drugs'>
                    <Link href={"/drugs"}>
                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('drugs') ? 'text-main-color' : 'text-second-color'}`}>
                            Препараты
                        </a>
                    </Link>
                </Menu.Item>
                <Menu.Item key={'equipments'}>
                    <Link href={"/equipments"}>
                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('equipments') ? 'text-main-color' : 'text-second-color'}`}>
                            Оборудование
                        </a>
                    </Link>
                </Menu.Item>
                {/*<Menu.Item key={'clinic'}>*/}
                {/*    <Link href={"/clinic"}>*/}
                {/*        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('clinic') ? 'text-main-color' : 'text-second-color'}`}>*/}
                {/*            Клиника*/}
                {/*        </a>*/}
                {/*    </Link>*/}
                {/*</Menu.Item>*/}
                <SubMenu key="sub1" title="Клиника">
                    <Menu.Item key="clinic">
                        <Link href={'/clinic'}>
                            <a>О нас</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="news">
                        <Link href={'/news'}>
                            <a>Новости</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="promo">
                        <Link href={`/promotions`}>
                            <a>Акции</a>
                        </Link>
                    </Menu.Item>
                </SubMenu>
                {/*<Menu.Item key={'promotions'}>*/}
                {/*    <Link href={"/promotions"}>*/}
                {/*        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('promotions') ? 'text-main-color' : 'text-second-color'}`}>*/}
                {/*            Акции*/}
                {/*        </a>*/}
                {/*    </Link>*/}
                {/*</Menu.Item>*/}
                {/*<Menu.Item key={'news'}>*/}
                {/*    <Link href={'/news'}>*/}
                {/*        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('news') ? 'text-main-color' : 'text-second-color'}`}>*/}
                {/*            Новости*/}
                {/*        </a>*/}
                {/*    </Link>*/}
                {/*</Menu.Item>*/}
                <Menu.Item key={'contacts'}>
                    <Link href={'/contacts'}>
                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('contacts') ? 'text-main-color' : 'text-second-color'}`}>
                            Контакты
                        </a>
                    </Link>
                </Menu.Item>
            </Menu>
            <hr/>

        </Drawer>
    )
}
