import Head from "next/head";
import Link from "next/link";
import React, {useState} from "react";
import {AiOutlineMenu, AiOutlinePhone} from 'react-icons/ai';
import MainNavigation from "./MainNavigation";
import { useRouter } from 'next/router'
import API from "../API/API";
import {Menu, Dropdown} from "antd";
import 'antd/dist/antd.css';
//import logo1 from '../../public/images/logo1.png';
import Image from "next/image";
//import logo2 from '../../public/images/logo2.png';

export default function PageLayout ({ children, title = 'Эстетическая медицина', description="Эстетическая медицина", keywords="Эстетическая медицина, косметологическая клиника" }) {
    const router = useRouter();
    const [services, setServices] = React.useState(null)
    const [visible, setVisible] = useState(false);

    function onClose () {
        setVisible(false);
    }

    function sideMenu () {
        setVisible(true);
    }


    const servicesMenu = (
        <Menu>
            {services?.content.map((item, index) => {
                return(
                    <Menu.Item key={index}>
                        <Link href={`/servicecategory/${item.hiddenName}`} key={index}>
                            <a className="text-second-color hover:text-main-color text-md">
                                {item.name}
                            </a>

                        </Link>
                    </Menu.Item>

                )
            })}
        </Menu>
    );

    const clinicMenu = (
        <Menu>
            <Menu.Item key="news">
                <Link href={`/news`} key="news">
                    <a className="text-second-color hover:text-main-color text-md">
                        Новости
                    </a>
                </Link>
            </Menu.Item>
            <Menu.Item key="promo">
                <Link href={`/promotions`} key="promo">
                    <a className="text-second-color hover:text-main-color text-md">
                        Акции
                    </a>
                </Link>
            </Menu.Item>
        </Menu>
    );


    React.useEffect(() => {
        API.get('/api/open-clinic-services/category')
            .then(function (response) {
                setServices(response.data.clinicServiceCategoryDataPage)
            })
    }, [])

    return (
        <>
            <Head>
                <title>{title} | Клиника Рассвет</title>
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="description" content={`${description} `} />
                <meta name="Keywords" content={`${keywords}, Тюмень, Клиника эстетической медицины, рассвет, косметологическая клиника`} />
            </Head>
            <div className="min-h-screen flex flex-col font-normal font-open-sans justify-between" >
                <nav className="py-6 px-5 md:px-0 md:w-[80%] md:mx-auto sticky uppercase top-0 z-50 bg-white">
                    <div className="flex justify-between items-center">
                        <Link href="/">
                            <a>
                                {/* <img className="h-10 cursor-pointer" src="https://i.ibb.co/BjJJbnh/firmlogo.png" alt="Главная страница"/> */}
                                <Image src="/images/logo1.png" height="40rem" width="180rem" alt="Косметология Рассвет"/>
                            </a>
                        </Link>
                        <ul className="lg:flex hidden items-center space-x-5 m-0">
                            <li className="transition duration-200 cursor-pointer">
                                <Link href={"/servicecategory"}>
                                    <Dropdown overlay={servicesMenu} placement="bottomCenter">
                                        <a className={`text-sm hover:text-main-color  ${router.asPath.includes('servicecategory') ? 'text-main-color' : 'text-second-color'}`}>
                                            Услуги
                                        </a>
                                    </Dropdown>
                                </Link>
                            </li>
                            <li className="transition duration-200 cursor-pointer">
                                <Link href={"/specialist"}>
                                    <a className={`text-sm hover:text-main-color  ${router.asPath.includes('specialist') ? 'text-main-color' : 'text-second-color'}`}>
                                        Специалисты
                                    </a>
                                </Link>
                            </li>
                            <li className="transition duration-200 cursor-pointer">
                                <Link href={"/equipments"}>
                                    <a className={`text-sm hover:text-main-color  ${router.asPath.includes('equipments') ? 'text-main-color' : 'text-second-color'}`}>
                                        Оборудование
                                    </a>
                                </Link>
                            </li>
                            <li className="hover:text-main-color transition duration-200 cursor-pointer">
                                <Link href={'/contacts'}>
                                    <a className={`text-sm hover:text-main-color  ${router.asPath.includes('contacts') ? 'text-main-color' : 'text-second-color'}`}>
                                        Контакты
                                    </a>
                                </Link>
                            </li>
                        </ul>
                        <div className="flex lg:hidden text-3xl">
                            <a href="tel://+73452530618">
                                <AiOutlinePhone className="mx-3 text-second-color hover:text-main-color"/>
                            </a>
                            <AiOutlineMenu onClick={()=>{sideMenu()}} className="hover:text-main-color hover:cursor-pointer" />
                        </div>
                    </div>
                </nav>

                <MainNavigation visible={visible} onClose={onClose} services={services}/>

                <main className="mx-auto px-5 md:px-0 md:w-[80%] flex-1">
                    { children }
                </main>


                <footer className="w-auto p-4 bottom-0 h-auto relative bg-second-color mt-8">
                    <div className="lg:mt-3 md:mx-12 lg:mx-28 grid lg:grid-cols-6 gap-10">
                        <div className="mb-4 lg:col-span-3">
                            <Image src="/images/logo2.png" height="40rem" width="180rem" alt="Косметология Рассвет"/>
                            <h3 className="text-white text-xl mb-1 mt-3">Остались вопросы?</h3>
                            <p className="text-gray-200 text-sm font-open-sans">
                                Вы можете узнать всю интересующую вас информацию по телефону, задать
                                вопрос в наших социальных сетях или записаться на индивидуальную консультацию.
                            </p>
                        </div>
                        <div className="mb-4 lg:mt-3 lg:col-span-1">
                            <div className=" flex lg:items-center ">
                                <div className="lg:flex lg:flex-wrap gap-2 lg:container lg:max-w-screen-lg">
                                    <a
                                        href={"https://www.instagram.com/rassvet_tmn"}
                                        target="_blank"
                                        rel="noreferrer"
                                        className="inline-flex items-center space-x-2 w-9 h-9"
                                    >
                                        <img src="/images/instagram.png" alt="Instagram" className="rounded-lg"/>
                                    </a>
                                    <a
                                        href={"https://go.2gis.com/wh2ll"}
                                        target="_blank"
                                        rel="noreferrer"
                                        className="inline-flex items-center space-x-2 w-9 h-9 rounded-lg ml-2"
                                    >
                                        <img src="/images/2gis.png" alt="2gis" className="rounded-lg"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="lg:mt-3 mb-4 font-open-sans lg:col-span-2">
                            {/*<h3 className="text-white font-bold mb-2 lg:mb-2"></h3>*/}
                            <div className="columns-2 md:columns-3 lg:columns-2 text-white">
                                {/*<Link href={'/clinic'}><a className="hover:text-main-color text-white">Клиника</a></Link><br/>*/}
                                <Link href={'/servicecategory'}><a className="hover:text-main-color text-white">Услуги</a></Link><br/>
                                <Link href={'/promotions'}><a className="hover:text-main-color text-white">Акции</a></Link><br/>
                                <Link href={'/news'}><a className="hover:text-main-color text-white">Новости</a></Link><br/>
                                <Link href={'/specialist'}><a className="hover:text-main-color text-white">Специалисты</a></Link><br/>
                                <Link href={'/contacts'}><a className="hover:text-main-color text-white">Контакты</a></Link><br/>
                                <Link href={'/'}><a className="hover:text-main-color text-white">Вернуться на главную</a></Link><br/>
                                {/*<Link href={'/cosmetology'}><a className="hover:text-main-color text-white">Косметология</a></Link><br/>*/}
                                {/*<Link href={'/privacypolicy'}><a className="hover:text-main-color text-white">Политика конфиденциальности</a></Link><br/>*/}
                                {/*<Link href={'/termsofuse'}><a className="hover:text-main-color text-white">Правила пользования</a></Link><br/>*/}
                            </div>
                        </div>
                    </div>
                    <div className="flex lg:justify-center border-t border-gray-300 pt-2 md:px-12 lg:px-28">
                        <div className="text-gray-400 mt-2">
                            <p className="mb-0">
                                Сделано с ❤️
                                <a
                                    href="https://idea-tmn.ru"
                                    className="text-white hover:text-main-color"
                                    rel="noreferrer"
                                    target="_blank"
                                >
                                    &laquo;IDEЯ&raquo;
                                </a>
                            </p>
                        </div>
                    </div>
                </footer>
            </div>
        </>

    )
}

export async function getServerSideProps(context) {
    const services = await (await fetch(`${process.env.NEXT_PUBLIC_BACKHOST}api/open-clinic-services/category`)).json()

    return {
        props: {
            services
        }
    }
}
