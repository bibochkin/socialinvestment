// import Image from "next/image";
// import InfiniteScroll from "react-infinite-scroll-component";
// import {notification, Spin} from "antd";
// import React, {useState} from "react";
// import moment from "moment";
// import API from "../API/API";
// import Link from "next/link";
//
// export default function TeamGrid(props) {
//     const [page, setPage] = useState(props.rawTeam.pageNumber);
//     const [teamPage, setTeamPage] = useState(props.rawTeam);
//     const [content, setContent] = useState(props.rawTeam.content);
//     const [moreData, setMoreData] = useState(true);
//
//     console.log(teamPage)
//
//     moment().locale('ru');
//
//     function moreSpecialists () {
//         if(teamPage.totalPages-1 > page)
//         {
//             API.get(`api/open-clinic-services/specialist?page=${page+1}`)
//                 .then(function(response){
//                     setPage(page+1);
//                     setTeamPage(response.data.specialistDataPage);
//                     let data = specialistsContent.concat(response.data.specialistDataPage.content);
//                     setContent(data);
//                 })
//                 .catch(function(error){
//                     notification['error']({
//                         message: 'Что-то пошло не так :(',
//                         description: error.message,
//                         className: "font-open-sans"
//                     })
//                 })
//         } else {
//             setMoreData(false);
//         }
//     }
//
//
//     return(
//         <section>
//             <InfiniteScroll
//                 next={()=>{moreSpecialists()}}
//                 hasMore={moreData}
//                 loader={()=><div className="w-full mx-auto"><Spin tip={"Загрузка данных..."} size={"large"}/></div>}
//                 dataLength={content.length}
//                 endMessage={()=><p className="w-full mx-auto">На этом пока всё!</p>}
//                 style={{overflow: 'visible', height: '100%'}}
//             >
//                 <div className="grid grid-cols-4 gap-10">
//                     {content?.map((item) => {
//                         return(
//                             <Link href={`specialist/${item.hiddenFullName}`} key={item.id}>
//                                 <a className="col-span-4 md:col-span-2 lg:col-span-1">
//                                     <div className=" border border-1 hover:shadow-lg cursor-pointer h-full">
//                                         <div className="py-5 px-10">
//                                             <Image alt={item.fullName}
//                                                    src={item.imagePath}
//                                                    className="rounded-full"
//                                                    width={500}
//                                                    height={500}
//                                                    objectFit="cover"
//                                             />
//                                         </div>
//                                         <div className="p-4 col-span-4 md:col-span-3">
//                                             <div className="h-full flex flex-col">
//                                                 <div className="row-span-4 flex-1 overflow-hidden">
//                                                     <h2 className="font-oswald font-bold text-xl md:text-2xl">{item.fullName}</h2>
//                                                     <p className="break-words text-black font-normal">
//                                                         {item.preview}
//                                                     </p>
//                                                 </div>
//                                             </div>
//
//
//                                         </div>
//                                     </div>
//                                 </a>
//                             </Link>
//                         )
//                     })}
//                 </div>
//             </InfiniteScroll>
//         </section>
//     )
// }
