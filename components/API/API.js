import axios from 'axios';

const API = axios.create({
    baseURL:  `https://backsunrise.idea-tmn.ru/`
});

export default API;
