import React, {useEffect, useState} from "react";
import Link from "next/link";
import moment from "moment";
import 'moment/locale/ru';
import {BsFillForwardFill} from "react-icons/bs";
import API from "../API/API";
import InfiniteScroll from "react-infinite-scroll-component";
import {notification, Spin} from "antd";
import Image from "next/image";
export default function NewsGrid (props) {

    const [page, setPage] = useState(0);
    const [newsPage, setNewsPage] = useState(props.newsList.newsPage);
    const [newsContent, setContent] = useState(props.newsList.newsPage.content);
    const [moreData, setMoreData] = useState(true);

    moment().locale('ru');

    function moreNews () {
        if(newsPage.totalPages-1 > page)
        {
            API.get(`api/openDynamicPages/news?page=${page+1}`)
                .then(function(response){
                    setPage(page+1);
                    setNewsPage(response.data.newsPage);
                    let data = newsContent.concat(response.data.newsPage.content);
                    setContent(data);
                })
                .catch(function(error){
                    notification['error']({
                        message: 'Что-то пошло не так :(',
                        description: error.message,
                        className: "font-open-sans"
                    })
                })
        } else {
            setMoreData(false);
        }
    }

    return (
        <div>
            {newsPage &&
            <InfiniteScroll
                next={()=>{moreNews()}}
                hasMore={moreData}
                loader={()=><div className="w-full mx-auto"><Spin tip={"Загрузка данных..."} size={"large"}/></div>}
                dataLength={newsContent.length}
                endMessage={()=><p className="w-full mx-auto">На этом пока всё!</p>}
                style={{overflow: 'visible', height: '100%'}}
            >
                <div
                    className="font-open-sans grid grid-cols-1 gap-12 mx-auto xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 px-1"
                >
                    {newsContent.map((data, index)=>{
                        return (
                            <Link href={`/news/${data.hiddenTitle}`} key={data.id}>
                                <a className={`w-full h-full rounded-sm shadow cursor-pointer hover:shadow-lg
                        ${index===0 ? "col-span-1 row-span-1 lg:col-span-2 lg:row-span-2" : ""}`}>
                                    <div

                                    >
                                        <Image alt={data.title} src={data.imagePath} width={1000} height={900} objectFit="cover"/>
                                        {/*<img alt={data.title} src={data.imagePath} className="w-[100%] h-[60%] object-cover rounded-t-sm"/>*/}
                                        <div
                                            className={`bg-second-color hover:bg-main-color text-white flex flex-row justify-between h-[5%] 
                            ${index===0 ? "text-xs px-2 lg:text-sm lg:px-4" : "text-xs px-2"}`}
                                        >
                                            <p className="my-auto">{moment(data.creationTime).format("LL")}</p>
                                            <p className="my-auto">{moment(data.creationTime).format("HH:mm")}</p>
                                        </div>
                                        <div className={`${index===0 ? "p-1 h-40 md:h-full lg:p-4 " : "p-1"} ${index !== 0 ? "h-40" : ""} text-black`}>
                                            <div className="m-0 p-0 h-[100%] flex flex-col">
                                                <div className="flex-1">
                                                    <div
                                                        className={`break-words font-semibold ${index===0 ? "text-lg lg:text-3xl" : "text-lg"} font-oswald`}
                                                    >
                                                        {data.title}
                                                    </div>
                                                    <div
                                                        className={`break-words ${index===0
                                                            ? "text-sm news-preview lg:text-xl lg:pt-3 lg:news-preview-first"
                                                            : "text-sm news-preview"} font-normal`}
                                                    >
                                                        {data.preview}
                                                    </div>
                                                </div>
                                                {/*<a className="text-black hover:text-main-color flex flex-row text-md border-t-2 border-second-color">*/}
                                                {/*    <BsFillForwardFill className="mr-3 text-xl"/>Читать полностью*/}
                                                {/*</a>*/}

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </Link>
                        )
                    })}
                </div>
            </InfiniteScroll>
            }
        </div>
    )
}
