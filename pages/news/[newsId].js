import React from "react";
import PageLayout from "../../components/Layouts/Layout";
import {useRouter} from "next/router";
import Image from "next/image";
import Link from "next/link";

export default function NewsPage ({newsData}) {
    const router = useRouter();
    // let currentNews = newsList.newsPage.content.find(obj => obj.id === Number(router.query.newsId));
    const news = newsData.newsData;
    return (
        <PageLayout title={news.title} description={news.preview} keywords={news.title}>
            <article>
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/news">
                        <Link href="/news">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Новости</span>
                            </a>
                        </Link>
                        <span className="mx-2">/</span>
                        <span itemProp="child" itemScope itemType={`https://rassvet72tmn.ru/news/${news.hiddenTitle}`}>
                            <Link href={`/news/${news.hiddenTitle}`}>
                                <a itemProp="url">
                                    <span itemProp="title" className="text-black">{news.title}</span>
                                </a>
                            </Link>
                        </span>
                    </span>
                  </span>
                </div>

                <div className="grid grid-cols-1 gap-12 my-10 lg:grid-cols-2">
                    <Image alt={news.title} src={news.imagePath} width={1000} height={500} objectFit="cover"/>
                    {/*<img alt={news.title} src={news.imagePath} className="rounded-md shadow-md lg:col-span-1" />*/}
                    <div className="">
                        <h1 className="text-3xl md:text-4xl font-oswald font-semibold">{news.title}</h1>
                        <p className="text-md md:text-2xl">{news.preview}</p>
                    </div>
                </div>
                <div className="text-lg text-black" dangerouslySetInnerHTML={{__html: news.content}}/>
            </article>

        </PageLayout>
    )
}

export async function getServerSideProps(context) {
    const newsData = await (await fetch(`${process.env.BACK_HOST}api/openDynamicPages/news/${context.query.newsId}`)).json()

    return {
        props: {
            newsData
        }
    }
}
