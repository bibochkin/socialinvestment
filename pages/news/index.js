import React from "react";
import PageLayout from "../../components/Layouts/Layout";
import 'moment/locale/ru';
import dynamic from "next/dynamic";
import Link from "next/link";

const DynamicComponent = dynamic(
    () => import("../../components/NewsComponents/NewsGrid")
);

export default function News ({newsList}) {

    return (
        <PageLayout title={"Новости"}>
            <article className="my-10">
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/news">
                        <Link href="/news">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Новости</span>
                            </a>
                        </Link>
                    </span>
                  </span>
                </div>
                <h1 className="text-3xl md:text-5xl font-bold font-oswald text-second-color">
                    НОВОСТИ
                </h1>
                <DynamicComponent newsList={newsList}/>
            </article>

        </PageLayout>
    )
}

export async function getServerSideProps(context) {
    const newsList = await (await fetch(`${process.env.BACK_HOST}api/openDynamicPages/news`)).json()

    return {
        props: {
            newsList
        }
    }
}
