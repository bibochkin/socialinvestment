import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {

    render() {
      return (
        <Html>
          <Head>
              <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
              <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;700&display=swap" rel="stylesheet" />
              <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Oswald:wght@300;700&display=swap" rel="stylesheet" />
              <meta charSet="utf-8"/>
          </Head>
          <body className='min-h-screen'>
            <Main />
            <NextScript />
          </body>
        </Html>
      )
    }
  }

  export default MyDocument
