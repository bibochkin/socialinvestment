import PageLayout from "../../components/Layouts/Layout";
import React, {useState} from "react";
import Link from "next/link";
import {CgArrowLongRight} from "react-icons/cg";
import ReviewForm from "../../components/Reviews/ReviewsForm";
import {Modal, notification, Tag} from "antd";
import API from "../../components/API/API";
import moment from "moment";
import 'moment/locale/ru';

export default function TeamPage({team, reviewList}) {
    const specialist = team.specialistData;
    const service = specialist.clinicServiceDataList;
    const drugs = specialist.drugDataList;
    const equipment = specialist.equipmentDataList;
    const reviews = reviewList.specialistReviewPage.content;
    const [visible, isVisible] = useState(false);
    moment().locale('ru');

    function onFinish(values) {
        values.specialistId = specialist.id;
        API.post('api/open-clinic-services/review', values)
            .then(function(response){
                notification["success"]({
                    message: response.data.message
                })
                isVisible(false);
            })
            .catch(function(error){
                notification["error"]({
                    message: "Что-то пошло не так :(",
                    description: error.data.message
                })
                isVisible(false);
            })
    }

    return (
        <PageLayout title={specialist.fullName} description={specialist.preview} keywords={`${specialist.fullName}, специалист, врач, косметолог`}>
            <article>
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/specialist">
                        <Link href="/specialist">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Специалисты</span>
                            </a>
                        </Link>
                        <span className="mx-2">/</span>
                        <span itemProp="child" itemScope itemType={`https://rassvet72tmn.ru/specialist/${specialist.hiddenFullName}`}>
                            <Link href={`/specialists/${specialist.hiddenFullName}`}>
                                <a itemProp="url">
                                    <span itemProp="title" className="text-black">{specialist.fullName}</span>
                                </a>
                            </Link>
                        </span>
                    </span>
                  </span>
                </div>

                <Modal
                    visible={visible}
                    footer={null}
                    onCancel={()=>{isVisible(false)}}
                >
                    <ReviewForm
                        onFinish={onFinish}
                        reviewType="specialist"
                        fullName={specialist.fullName}
                    />
                </Modal>

                <div className="grid font-normal gap-12 my-10 lg:grid-cols-3">
                    <div className="grid lg:col-span-3 lg:grid-cols-3 lg:gap-10">
                        <img
                            alt={specialist.fullName}
                            src={specialist.imagePath}
                            className="col-span-1 w-full h-auto rounded-sm"
                        />
                        <div className="lg:col-span-2 h-full flex flex-col justify-between">
                            <div className="mt-6 md:mt-0">
                                <h1 className="text-3xl md:text-4xl font-oswald">{specialist.fullName}</h1>
                                <p className="text-second-color text-md text-black font-normal md:text-2xl">{specialist.preview}</p>
                                <div className="text-md text-black" dangerouslySetInnerHTML={{__html: specialist.content}}/>
                            </div>

                            <a href="https://app.arnica.pro/booking/booking?&orgid=47124#/employees" target="_blank" rel="noreferrer">
                                <button className="border text-black border-second-color p-2 md:w-60 hover:border-main-color hover:shadow-lg text-lg ease-in duration-200">
                                    Записаться на приём
                                </button>
                            </a>
                        </div>
                    </div>
                    <div className="lg:col-span-2 lg:col-start-2">
                        {service.length > 0 &&
                        <div>
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald mb-5 md:mb-10">Услуги</h2>
                            {service.map((item)=>{
                                return (
                                    <Link href={`/servicecategory/${item.clinicServiceCategory.hiddenName}/${item.hiddenName}`} key={item.id}>
                                        <div
                                            className="flex justify-between border-b border-b-gray-400 mb-10 cursor-pointer hover:text-main-color ease-in duration-100"
                                            key={item.id}
                                        >
                                            <h3 className="text-xl md:text-3xl font-normal hover:text-main-color ease-in duration-100">{item.name}</h3>
                                            <div className="text-3xl  hover:text-main-color ease-in duration-100">
                                                <CgArrowLongRight/>
                                            </div>
                                        </div>
                                    </Link>
                                )
                            })}
                        </div>
                        }
                        {drugs.length > 0 &&
                        <div>
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald mb-5 md:mb-10 lg:col-span-2">Препараты</h2>
                            {drugs.map((item)=>{
                                return (
                                    <Link href={`/drugs/${item.hiddenName}`} key={item.id}>
                                        <div
                                            className="flex justify-between border-b border-b-gray-400 mb-10 cursor-pointer hover:text-main-color ease-in duration-100"
                                            key={item.id}
                                        >
                                            <h3 className="text-xl md:text-3xl font-normal hover:text-main-color ease-in duration-100">{item.name}</h3>
                                            <div className="text-3xl  hover:text-main-color ease-in duration-100">
                                                <CgArrowLongRight/>
                                            </div>
                                        </div>
                                    </Link>
                                )
                            })}
                        </div>
                        }
                        {equipment.length > 0 &&
                        <div className="grid gap-x-10 my-6 md:grid-cols-2 lg:grid-cols-3 lg:gap-2">
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald md:mb-10 lg:col-span-3 md:col-span-2">Оборудование</h2>
                            {equipment.map((item, index)=>{
                                return (
                                    <section
                                        key={index}
                                        className="font-open-sans rounded-sm border-2 shadow-gray-300 mb-4"
                                    >
                                        <div className="">
                                            <img
                                                src={item.imagePath}
                                                alt={item.name}
                                                className="h-56 w-full rounded-t-sm object-cover"
                                            />
                                            <div className="p-4 h-40 flex flex-col justify-between m-0">
                                                <h1 className="font-normal text-xl mb-1 md:text-md xl:text-xl">
                                                    {item.name}
                                                </h1>
                                                <Link href={`/equipments/${item.hiddenName}`}>
                                                    <a className="underline text-second-color hover:text-main-color text-lg">Подробнее</a>
                                                </Link>
                                            </div>
                                        </div>
                                    </section>
                                )
                            })}
                        </div>
                        }
                        {reviews.length > 0 &&
                        <div className="lg:col-span-3">
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald md:mb-6 lg:col-span-3 md:col-span-2">Отзывы</h2>
                            {reviews.map((item)=>{
                                return(
                                    <div className="border p-4 mb-4" key={item.id}>
                                        <h3 className="font-oswald text-xl text-second-color font-semibold">{item.reviewerFullName}</h3>
                                        <Tag color="gray">{moment(item.creationTime).format("LL")}</Tag>
                                        <p className="text-lg my-4">{item.content}</p>
                                    </div>
                                )
                            })}
                        </div>
                        }
                        <button
                            className="border border-second-color mt-6 p-2 md:w-60 hover:border-main-color hover:shadow-lg text-lg ease-in duration-200"
                            onClick={()=>{isVisible(true)}}
                        >
                            Оставить отзыв
                        </button>
                    </div>
                </div>
            </article>
        </PageLayout>
    )
}

export async function getServerSideProps(context) {
    const team = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/specialist/${context.query.specialistId}`)).json();
    const reviewList = await (await fetch(`${process.env.BACK_HOST}/api/open-clinic-services/review/specialist/${team.specialistData.id}`)).json();

    return {
        props: {
            team,
            reviewList
        }
    }
}
