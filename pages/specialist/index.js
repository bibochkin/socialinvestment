import React from "react";
import PageLayout from "../../components/Layouts/Layout";
import 'moment/locale/ru';
import Link from "next/link";
import Image from "next/image";

export default function Specialist ({team}) {

    const content = team.specialistDataPage.content;

    return (
        <PageLayout title={"Специалисты"}
                    description={"Специалисты клиники – признанные эксперты в своей области. Благодаря огромному практическому опыту врачи эффективно решают эстетические задачи пациентов."}
                    keywords={"Специалисты в косметолгической клинике, врачи косметологии,"}
        >
            <article className="my-10">
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/specialist">
                        <Link href="/specialist">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Специалисты</span>
                            </a>
                        </Link>
                    </span>
                  </span>
                </div>

                <h1 className="text-3xl md:text-5xl font-bold font-oswald text-second-color">
                    Специалисты
                </h1>

                <p className="md:w-2/4 text-justify">
                    Специалисты клиники – признанные эксперты в своей области.
                    Большинство из них активно занимается научной деятельностью и имеет высшее квалифицированное образование.
                    Благодаря огромному практическому опыту врачи эффективно решают эстетические задачи пациентов.
                    Наши специалисты постоянно проходят обучения и повышают свою квалификацию.
                </p>


                <div className="grid grid-cols-4 gap-10 mb-5">
                    {content?.map((item) => {
                        return(
                            <Link href={`specialist/${item.hiddenFullName}`} key={item.id}>
                                <a className="group col-span-4 md:col-span-2 lg:col-span-1 hover:bg-second-color duration-200 text-black transition">
                                    <div className=" border border-1 hover:shadow-lg cursor-pointer h-full">
                                        <div className="py-5 px-10">
                                            <Image alt={item.fullName}
                                                   src={item.imagePath}
                                                   className="rounded-full"
                                                   width={500}
                                                   height={500}
                                                   objectFit="cover"
                                            />
                                        </div>
                                        <div className="p-4 col-span-4 md:col-span-3">
                                            <div className="h-full flex flex-col">
                                                <div className="row-span-4 flex-1 overflow-hidden">
                                                    <h2 className="font-oswald font-bold text-xl md:text-2xl group-hover:text-main-color">{item.fullName}</h2>
                                                    <p className="break-words font-normal text-black group-hover:text-test-1">
                                                        {item.preview}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </Link>
                        )
                    })}
                </div>

                <hr className="my-5"/>

                <h2 className="text-2xl md:text-4xl font-bold font-oswald text-second-color">
                    Специалисты клиники
                </h2>

                <p className="text-justify">
                    Ищете клинику эстетической косметологии в Тюмени, где работают опытные специалисты и используются
                    самые современные методики? Квалифицированные врачи Рассвет помогут избавиться от возрастных изменений
                    и косметических дефектов. Мы предоставим индивидуальный подход, приемлемые цены и высокое качество
                    обслуживания.
                </p>
            </article>

        </PageLayout>
    )
}

export async function getServerSideProps(context) {
    const team = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/specialist?size=999`)).json()

    return {
        props: {
            team
        }
    }
}
