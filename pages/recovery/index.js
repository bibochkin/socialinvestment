import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";
import {Input, Form, Button, notification} from "antd";
import 'antd/dist/antd.css';
import API from "../../components/API/API";

export default function Recovery () {

    const router = useRouter();
    const [visible, isVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const params = new URLSearchParams(`http://localhost:3001${router.asPath}`)

    useEffect(()=>{
        if(params.get('token') !== null) {
            isVisible(true);
        } else {
            isVisible(false);
            router.push('/')
        }
    })

    function onFinish (values) {
        setLoading(true);
        if (values.password === values.confirmPassword) {
            values.token = router.query.token;
            API.post('api/auth/change-password', values)
                .then(function(response){
                    notification['success']({
                        message: response.data.message
                    })
                    router.push('/admin')
                })
                setLoading(false)
                .catch(function(error){
                    notification['error']({
                        message: 'Что-то пошло не так :(',
                        description: error.message,
                        className: "font-open-sans"
                    })
                    setLoading(false)
                })
        } else {
            notification['error']({
                message: 'Пароли не совпадают!',
                className: "font-open-sans"
            })
            setLoading(false)
        }
    }

    return (
        <div className="min-h-screen w-[100%] flex flex-col justify-center bg-gray-200 font-open-sans 2xl:px-[35%] xl:px-[30%] lg:px-[25%] md:px-[20%] sm:px-[10%]">
            {visible &&
            <div className="bg-white h-auto mx-auto my-auto py-4 px-10">
                <Form
                    name="base"
                    onFinish={onFinish}
                    autoComplete="off"
                    className="bg-white"
                    layout="vertical"
                >
                    <h1 className="text-2xl font-oswald text-center mb-3 border-b-2 pb-2 border-black">Введите новый пароль</h1>
                    <Form.Item
                        label="Пароль"
                        required rules={[{ required: true, message: 'Пожалуйста, введите пароль' }]}
                        name="password"
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label="Повторите пароль"
                        required rules={[{ required: true, message: 'Пожалуйста, введите пароль' }]}
                        name="confirmPassword"
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            loading={loading}
                        >
                            Обновить пароль
                        </Button>
                    </Form.Item>
                </Form>
            </div>
            }
        </div>
    )
}