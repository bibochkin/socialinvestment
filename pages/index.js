import PageLayout from '../components/Layouts/Layout';
import Carousel from '../components/MainComponents/Carousel';
import Services from '../components/MainComponents/Services';
import Promotions from "../components/MainComponents/Promotions";
import MainPageAbout from "../components/MainComponents/About";
import Training from "../components/MainComponents/Training";


export default function Index ({promotions, services, specialists}) {

    return (
        <PageLayout
            title={"Главная страница | Косметология"}
            description={'Клиника красоты «Рассвет» предлагает широкий спектр косметологических услуг, которые проводятся профессиональными косметологами и врачами.'}
            keywords={'Косметологические услуги'}
        >

            <Promotions promotions={promotions.promotionPage}/>
            <Services services={services.clinicServiceCategoryDataPage}/>
            <MainPageAbout />
            <Carousel specialists={specialists.specialistDataPage}/>
            <Training />

        </PageLayout>
    )
}


export async function getServerSideProps(context) {
    const promotions = await (await fetch(`${process.env.BACK_HOST}api/openDynamicPages/promotion`)).json();
    const services = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/category?size=4`)).json();
    const specialists = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/specialist?size=5`)).json();


    return {
        props: {
            promotions,
            services,
            specialists
        }
    }
}
