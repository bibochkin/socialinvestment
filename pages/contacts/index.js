import PageLayout from "../../components/Layouts/Layout"
import React from "react";
import {BiPhone, BiBuildingHouse} from 'react-icons/bi';
import {AiOutlineClockCircle} from 'react-icons/ai';

export default function Contacts () {
    return (
        <PageLayout
            title={"Контакты"}
            description={'Клиника красоты «Рассвет» предлагает широкий спектр косметологических услуг, которые проводятся профессиональными косметологами, врачами и визажистами.'}
            keywords={'Клиника эстетической медицины, косметологические услуги, эстетическая медицина'}
        >

            <h2 className="text-3xl text-second-color mb-6 lg:mb-10 lg:mt-6">Контакты</h2>

            <div className="grid lg:grid-cols-2 gap-10">
                <div>
                    <div className="flex flex-row mb-6">
                        <BiBuildingHouse className="text-main-color text-3xl"/>
                        <div className="text-md lg:text-xl ml-4">г.Тюмень, улица Максима Горького, 90, 2 этаж</div>
                    </div>
                    <div className="flex flex-row mb-6">
                        <BiPhone className="text-main-color text-3xl"/>
                        <div className="text-md lg:text-xl ml-4">{"+7(3452) 53‒06‒18"}</div>
                    </div>
                    <div className="flex flex-row mb-6">
                        <AiOutlineClockCircle className="text-main-color text-3xl"/>
                        <div className="text-md lg:text-xl ml-4">Ежедневно с 10:00 до 20:00</div>
                    </div>
                </div>
                <div className="w-full">
                    <iframe
                        src="https://yandex.ru/map-widget/v1/?um=constructor%3Ade38751f6f6e30790758fd4d3d209e1dbe572da1f13803f36df164613699019b&amp;source=constructor"
                        width="100%" height="720" frameBorder="0"/>
                </div>
            </div>


        </PageLayout>
    )
}