import Link from "next/link";
import React from "react";
import Image from "next/image";
import PageLayout from "../../../components/Layouts/Layout";

export default function index({service}) {
    const rawService = service.clinicServiceCategoryData
    return(
        <PageLayout title={rawService.name}
                    keywords={"услуги, " + rawService.name}
                    description={rawService.preview}
        >
            <article>
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/servicecategory">
                        <Link href="/servicecategory">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Услуги</span>
                            </a>
                        </Link>
                        <span className="mx-2">/</span>
                        <span itemProp="child" itemScope itemType={`https://rassvet72tmn.ru/servicecategory/${rawService.hiddenName}`}>
                            <Link href={`/servicecategory/${rawService.hiddenName}`}>
                                <a itemProp="url">
                                    <span itemProp="title" className="text-black">{rawService.name}</span>
                                </a>
                            </Link>
                        </span>
                    </span>
                  </span>
                </div>

                <h1 className="text-3xl md:text-5xl font-bold font-oswald text-second-color">{rawService.name}</h1>

                <div className="grid grid-cols-4 gap-20 mt-10">
                    <div className="col-span-4 md:col-span-2">
                        <div className="text-md md:text-lg text-black" dangerouslySetInnerHTML={{__html: rawService.content}}/>
                    </div>
                </div>

                <div className="grid grid-cols-4 gap-10 mt-10">
                    {rawService.clinicServiceDataList.map((item, index) => {
                        return(
                            <Link href={`${rawService.hiddenName}/${item.hiddenName}`} key={item.id}>
                                <a className={`group col-span-4 md:col-span-3 lg:col-span-1 hover:bg-second-color duration-200 transition`}>
                                    <div className="border border-1 hover:shadow-lg cursor-pointer h-52 px-5">
                                        <h2 className="mt-5 font-oswald text-3xl group-hover:text-main-color">{item.name}</h2>
                                        <p className="text-black group-hover:text-test-1">
                                            {item.preview}
                                        </p>
                                    </div>
                                </a>
                            </Link>
                        )
                    })}
                </div>
            </article>
        </PageLayout>
    )
}


export async function getServerSideProps(context) {
    const service = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/category/${context.query.categoryId}`)).json()

    return {
        props: {
            service
        }
    }
}


