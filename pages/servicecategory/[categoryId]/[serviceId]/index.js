import PageLayout from "../../../../components/Layouts/Layout";
import Link from "next/link";
import {CgArrowLongRight} from "react-icons/cg";
import React from "react";

export default function index({service}) {
    const rawService = service.clinicServiceData;
    const services = rawService.clinicServiceDataList;
    const drugs = rawService.drugDataList;
    const equipment = rawService.equipmentDataList;
    const specialists = rawService.specialistDataList;

    return(
        <PageLayout title={rawService.name}
                    keywords={"услуги, " + rawService.name}
                    description={rawService.preview}
        >
            <article>
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/servicecategory">
                        <Link href="/servicecategory">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Услуги</span>
                            </a>
                        </Link>
                        <span className="mx-2">/</span>
                        <span itemProp="child" itemScope itemType={`https://rassvet72tmn.ru/servicecategory/${rawService.clinicServiceCategory.hiddenName}`}>
                            <Link href={`/servicecategory/${rawService.clinicServiceCategory.hiddenName}`}>
                                <a itemProp="url">
                                    <span itemProp="title" className="text-black">{rawService.clinicServiceCategory.name}</span>
                                </a>
                            </Link>
                            <span className="mx-2">/</span>
                            <span itemProp="child" itemScope itemType={`https://rassvet72tmn.ru/servicecategory/${rawService.clinicServiceCategory.hiddenName}/${rawService.hiddenName}`}>
                                <Link href={`/servicecategory/${rawService.clinicServiceCategory.hiddenName}/${rawService.hiddenName}`}>
                                    <a itemProp="url">
                                        <span itemProp="title" className="text-black">{rawService.name}</span>
                                    </a>
                                </Link>
                            </span>
                        </span>
                    </span>
                  </span>
                </div>


                <h1 className="text-3xl md:text-5xl font-bold font-oswald text-second-color">{rawService.name}</h1>

                <div className="grid font-normal gap-12 my-10 lg:grid-cols-3">
                    <div className="grid lg:col-span-3 lg:grid-cols-3 lg:gap-10">
                        <img
                            alt={rawService.name}
                            src={rawService.imagePath}
                            className="col-span-1 w-full h-auto rounded-sm"
                        />
                        <div className="lg:col-span-2 h-full flex flex-col justify-between">
                            <div className="mt-6 md:mt-0">
                                <h1 className="text-3xl md:text-4xl font-oswald">{rawService.name}</h1>
                                <p className="text-second-color mb-4 text-md text-black font-normal md:text-2xl">{rawService.preview}</p>
                                {rawService.price && <p className="font-semibold text-main-color">{`Стоимость услуги: ${rawService.price} р`}</p>}
                                <div className="text-md text-black" dangerouslySetInnerHTML={{__html: rawService.content}}/>
                            </div>
                            <a href="https://app.arnica.pro/booking/booking?&orgid=47124#/services" target="_blank" rel="noreferrer">
                                <button className="border-2 text-black p-2 md:w-60 hover:border-main-color hover:shadow-lg text-lg ease-in duration-200">
                                    Записаться на приём
                                </button>
                            </a>
                        </div>
                    </div>
                    <div className="lg:col-span-2 lg:col-start-2">
                        {services.length > 0 &&
                        <div>
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald mb-5 md:mb-10">Услуги</h2>
                            {services.map((item)=>{
                                return (
                                    <Link href={`/servicecategory/${item.hiddenName}`} key={item.id}>
                                        <div
                                            className="flex justify-between border-b border-b-gray-400 mb-10 cursor-pointer hover:text-main-color ease-in duration-100"
                                            key={item.id}
                                        >
                                            <h3 className="text-xl md:text-3xl font-normal hover:text-main-color ease-in duration-100">{item.name}</h3>
                                            <div className="text-3xl  hover:text-main-color ease-in duration-100">
                                                <CgArrowLongRight/>
                                            </div>
                                        </div>
                                    </Link>
                                )
                            })}
                        </div>
                        }

                        {specialists.length > 0 &&
                        <div>
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald mb-5 md:mb-10">Специалисты</h2>
                            {specialists.map((item)=>{
                                return (
                                    <Link href={`/specialist/${item.hiddenFullName}`} key={item.id}>
                                        <a className="flex justify-between border-b border-b-gray-400 mb-10 cursor-pointer hover:text-main-color ease-in duration-100">
                                            <h3 className="text-xl md:text-3xl font-normal hover:text-main-color ease-in duration-100">{item.fullName}</h3>
                                            <div className="text-3xl text-black hover:text-main-color ease-in duration-100">
                                                <CgArrowLongRight />
                                            </div>
                                        </a>

                                    </Link>
                                )
                            })}
                        </div>
                        }
                        {drugs.length > 0 &&
                        <div>
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald mb-5 md:mb-10 lg:col-span-2">Препараты</h2>
                            {drugs.map((item)=>{
                                return (
                                    <Link href={`/drugs/${item.hiddenName}`} key={item.id}>
                                        <div
                                            className="flex justify-between border-b border-b-gray-400 mb-10 cursor-pointer hover:text-main-color ease-in duration-100"
                                            key={item.id}
                                        >
                                            <h3 className="text-xl md:text-3xl font-normal hover:text-main-color ease-in duration-100">{item.name}</h3>
                                            <div className="text-3xl  hover:text-main-color ease-in duration-100">
                                                <CgArrowLongRight/>
                                            </div>
                                        </div>
                                    </Link>
                                )
                            })}
                        </div>
                        }
                        {equipment.length > 0 &&
                        <div className="grid gap-x-10 my-6 md:grid-cols-2 lg:grid-cols-3 lg:gap-2">
                            <h2 className="text-main-color text-3xl md:text-4xl font-oswald md:mb-10 lg:col-span-3 md:col-span-2">Оборудование</h2>
                            {equipment.map((item, index)=>{
                                return (
                                    <section
                                        key={index}
                                        className="font-open-sans rounded-sm border-2 shadow-gray-300 mb-4"
                                    >
                                        <div className="">
                                            <img
                                                src={item.imagePath}
                                                alt={item.name}
                                                className="h-56 w-full rounded-t-sm object-cover"
                                            />
                                            <div className="p-4 h-40 flex flex-col justify-between m-0">
                                                <h1 className="font-normal text-xl mb-1 md:text-md xl:text-xl">
                                                    {item.name}
                                                </h1>
                                                <Link href={`/equipment/${item.hiddenName}`}>
                                                    <a className="underline text-second-color hover:text-main-color text-lg">Подробнее</a>
                                                </Link>
                                            </div>
                                        </div>
                                    </section>
                                )
                            })}
                        </div>
                        }
                    </div>
                </div>

            </article>
        </PageLayout>
    )
}


export async function getServerSideProps(context) {
    const service = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/service/${context.query.serviceId}`)).json()

    return {
        props: {
            service
        }
    }
}

