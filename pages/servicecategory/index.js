import PageLayout from "../../components/Layouts/Layout";
import Link from "next/link";
import Image from "next/image";
import React from "react";

export default function servicecategory ({services}) {
    const rawServices = services.clinicServiceCategoryDataPage;

    return (
        <PageLayout title={"Медицинские услуги"}
                    description={"Наша клиника работает по принципу индивидуального подхода к каждому клиенту. Мы уважаем ваши пожелания и предпочтения. Для нас важно, чтобы клиенты чувствовали себя комфортно."}
                    keywords={"Услуги, услуги косметологической клиники"}
        >
            <article className="my-10">
                <div className="breadcrumbs font-light my-5">
                  <span itemScope itemType="https://rassvet72tmn.ru">
                      <Link href="/">
                        <a itemProp="url">
                          <span itemProp="title" className="text-black">Главная</span>
                        </a>
                      </Link>
                      <span className="mx-2">/</span>
                      <span itemProp="child" itemScope itemType="https://rassvet72tmn.ru/servicecategory">
                        <Link href="/servicecategory">
                            <a itemProp="url">
                                <span itemProp="title" className="text-black">Услуги</span>
                            </a>
                        </Link>
                    </span>
                  </span>
                </div>

                <h1 className="text-3xl md:text-5xl font-bold font-oswald text-second-color">
                    Услуги
                </h1>

                <p className="md:w-2/4 text-justify">
                    {`Чтобы подобрать подходящую именно вам процедуру,
                        обратитесь в нашу клинику красоты "Рассвет". Вы можете записаться
                        на процедуру онлайн. Нас выбирают те, кто ценит безопасность, эффективность и эстетику`}
                </p>

                <div className={`grid grid-cols-${rawServices.content.length > 4 ? "4" : rawServices.content.length} gap-10`}>
                    {rawServices?.content.map((item) => {
                        return(
                            <Link href={`servicecategory/${item.hiddenName}`} key={item.id}>
                                <a className={`group col-span-4 md:col-span-3 lg:col-span-1 hover:bg-second-color duration-200 transition`}>
                                    <div className="border border-1 hover:shadow-lg cursor-pointer h-52 px-5 pt-10">
                                        <h2 className="font-oswald text-3xl group-hover:text-main-color">{item.name}</h2>
                                        <p className="text-black group-hover:text-test-1">
                                            {item.preview}
                                        </p>
                                    </div>
                                </a>
                            </Link>
                        )
                    })}
                </div>

                <hr className="my-5"/>

                <h2 className="text-2xl md:text-4xl font-bold font-oswald text-second-color">
                    Услуги клиники
                </h2>

                <p className="text-justify">
                    Наша клиника работает по принципу индивидуального подхода к каждому клиенту.
                    Мы уважаем ваши пожелания и предпочтения. Для нас важно, чтобы клиенты чувствовали себя комфортно.
                    Вот почему мы всегда следим за чистотой и атмосферой нашей клиники.
                </p>
            </article>
        </PageLayout>
    )
}

export async function getServerSideProps(context) {
    const services = await (await fetch(`${process.env.BACK_HOST}api/open-clinic-services/category`)).json()

    return {
        props: {
            services
        }
    }
}
