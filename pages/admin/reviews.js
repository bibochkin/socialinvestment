import React, {useEffect, useState} from 'react';
import AdminLayout from "../../components/AdminComponents/AdminLayout";
import {Drawer, notification, PageHeader, Popconfirm, Table, Tag, Tooltip} from "antd";
import API from "../../components/API/API";
import {useRouter} from "next/router";
import moment from "moment";
import {BsArrowsFullscreen, BsTrash, BsXLg, BsCheckLg} from "react-icons/bs";

export default function Reviews (props) {

    const router = useRouter();
    const [token, setToken] = useState(null);
    const [dataSource, setData] = useState(null);
    const [currentData, setCurrentData] = useState(null);
    const [drawerData, setDrawerData] = useState(null);
    const [drawer, isDrawer] = useState(false);

    useEffect(()=>{
        if (!localStorage['token']) {
            router.push('/admin');
        } else {
            setToken(localStorage['token']);
        }
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (dataSource === null) {
            updateData();
        }
    }, []);

    function updateData () {
        API.get('api/clinic-services/review?size=10')
            .then(function(response){
                setData(response.data.reviewDataPage.content);
            })
            .catch(function(error){
                notification["error"]({
                    message: "Что-то пошло не так :("
                })
            })
    }

    function deleteReview (item) {
        API.delete(`api/clinic-services/review/${item}`)
            .then(function(response){
                notification["success"]({
                    message: response.data.message
                })
                updateData();
            })
            .catch(function(error){
                notification["error"]({
                    message: "Что-то пошло не так :("
                })
            })
    }

    function changeStatus (id, accept) {
        const body = {
            reviewId: id,
            accept: accept
        }
        API.put("api/clinic-services/review", body)
            .then(function(response){
                notification["success"]({
                    message: response.data.message
                })
                updateData();
            })
            .catch(function(error){
                notification["error"]({
                    message: "Что-то пошло не так :("
                })
            })
    }

    const columns = [
        {
            title: "Имя",
            dataIndex: "reviewerFullName",
            key: "1",
        },
        {
            title: "email",
            dataIndex: "reviewerEmail",
            key: "2",
            render: (item)=>{
                if (!item) {
                    return <span>Отсутствует</span>
                } else {
                    return <span>{item}</span>
                }
            }
        },
        {
            title: "Дата публикации",
            dataIndex: "creationTime",
            key: "3",
            render: item => moment(item).format("DD.MM.YYYY | HH:mm")
        },
        {
            title: "Отзыв",
            dataIndex: "content",
            key: "4",
            render: item => <p className="break-words news-preview w-128">{item}</p>
        },
        {
            title: "Статус",
            dataIndex: "accepted",
            key: "5",
            render: item => item ? <Tag color="green">Принят</Tag> : <Tag color="red">Не принят</Tag>
        },
        {
            title: "Действия",
            dataIndex: "id",
            key: "6",
            width: "10%",
            render: (item)=>{
                let current = dataSource.find(key => key.id === item)
                return (
                    <div className="flex flex-row justify-between font-open-sans" key={item}>
                        <Tooltip title="Просмотр">
                            <BsArrowsFullscreen
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=>{
                                    setDrawerData(dataSource.find(key => key.id === item));
                                    isDrawer(true);
                                }}
                            />
                        </Tooltip>
                        {current.accepted &&
                        <Tooltip title="Отклонить" >
                            <BsXLg
                                className="text-white bg-red-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=>{changeStatus(item, false)}}
                            />
                        </Tooltip>
                        }
                        {!current.accepted &&
                        <Tooltip title="Принять" >
                            <BsCheckLg
                                className="text-white bg-green-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=>{changeStatus(item, true)}}
                            />
                        </Tooltip>
                        }
                        <Popconfirm
                            title="Вы уверены, что хотите удалить этот отзыв?"
                            onConfirm={()=>{deleteReview(item)}}
                            okText="Удалить"
                            okType="danger primary"
                            cancelText="Отмена"
                            placement="leftTop"
                        >
                            <Tooltip title="Удалить">
                                <BsTrash
                                    className="text-white bg-red-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                    id={item}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                )
            }
        }
    ];

    return (
        <div>
            {token &&
            <AdminLayout>
                <PageHeader
                    title="Отзывы"
                    className="border-b-2 border-blue-600"
                />
                <Drawer
                    placement="right"
                    onClose={()=>{
                        isDrawer(false);
                        setDrawerData(null);
                    }}
                    visible={drawer}
                    width="50%"
                >
                    <section className="font-open-sans mx-2 md:mx-10 lg:mx-15 xl:mx-20">
                        {drawerData &&
                        <div className="p-6">
                            <Tag color="grey" className="m-1 rounded-md">{moment(drawerData.creationTime).format("DD.MM.YYYY   HH:mm")}</Tag>
                            <div className="mt-4">
                                <h1 className="text-lg font-oswald font-bold md:text-xl lg:text-2xl xl:text-3xl">
                                    {"specialistData" in drawerData
                                        ? `Отзыв на специалиста: ${drawerData.specialistData.fullName}`
                                        : `Отзыв на услугу: ${drawerData.serviceData.name}`}
                                </h1>
                                <h3 className="text-md md:text-lg lg:text-xl xl:text-2xl">
                                    {drawerData.reviewerFullName}
                                </h3>
                                {drawerData.reviewerEmail.length !== 0 &&
                                <Tag color="blue">{drawerData.reviewerEmail}</Tag>
                                }
                            </div>
                            <div>
                                <p className="text-lg mt-4">{drawerData.content}</p>
                            </div>
                        </div>
                        }
                    </section>
                </Drawer>
                <Table
                    dataSource={dataSource}
                    columns={columns}
                    bordered
                    className="my-4"
                    size="small"
                    pagination={{position: ["bottomCenter"]}}
                />
            </AdminLayout>
            }
        </div>
    )
}