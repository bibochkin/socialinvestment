import React, {useEffect, useState} from "react";
import {Button, Collapse, Drawer, notification, PageHeader, Popconfirm, Table, Tag, Tooltip} from "antd";
import 'antd/dist/antd.css';
import {Modal} from "antd";
import AdminLayout from "../../components/AdminComponents/AdminLayout";
import {useRouter} from "next/router";
import API from "../../components/API/API";
import AddClinicRecord from "../../components/AdminComponents/AddClinicRecord";
import {ImPlus} from "react-icons/im";
import moment from "moment";
import {BsArrowsFullscreen, BsPencil, BsTrash} from "react-icons/bs";
import EditRecord from "../../components/AdminComponents/EditRecord";
import EditClinicRecord from "../../components/AdminComponents/EditClinicRecord";


export default function Team () {

    const [visible, isVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [data, setData] = useState(null);
    const [content, setContent] = useState(null);
    const [currentValues, setCurrentValues] = useState(null);
    const [currentSpecialist, setCurrentSpecialist] = useState(null);
    const [editVisible, isEditVisible] = useState(false);
    const [drawer, isDrawer] = useState(false);
    const [drawerTeam, setDrawerTeam] = useState(null);
    const [collapse, isCollapse] = useState(false);
    const router = useRouter();

    useEffect(()=>{
        if (!localStorage['token']) {
            router.push('/admin');
        } else {
            setToken(localStorage['token']);
        }
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (data === null) {
            updateData();
        }
    }, []);

    function updateData () {
        API.get('api/clinic-services/specialist')
            .then(function (response){
                setData(response.data.specialistDataPage);
                setContent(response.data.specialistDataPage.content);
            })
            .catch(function (error){
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.data.message
                })
            })
    }

    function deleteSpecialist (id) {
        API.delete(`api/clinic-services/specialist/${id}`)
            .then((response)=>{
                notification['success']({
                    message: response.data.message,
                    className: "font-open-sans"
                })
                updateData();
            })
            .catch((error)=>{
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    const columns = [
        {
            title: "ФИО",
            dataIndex: "fullName",
            key: "1",
            width: "25%",
        },
        {
            title: "Канонический URL",
            dataIndex: "hiddenFullName",
            key: "10",
            width: "25%"
        },
        {
            title: "Краткое описание",
            dataIndex: "preview",
            key: "2",
            width: "25%",
        },
        {
            title: "Главное изображение",
            dataIndex: "imagePath",
            key: "5",
            width: "15%",
            render: (item)=>{
                return <img key={item} alt="photo" src={item} className="max-h-14 w-auto mx-auto"/>
            }
        },
        {
            title: "Действия",
            dataIndex: "id",
            key: "6",
            width: "10%",
            render: (item)=>{
                return (
                    <div className="flex flex-row justify-between font-open-sans" key={item}>
                        <Tooltip title="Просмотр">
                            <BsArrowsFullscreen
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=>{
                                    setDrawerTeam(content.find(key => key.id === item));
                                    isDrawer(true);
                                }}
                            />
                        </Tooltip>
                        <Tooltip title="Редактировать">
                            <BsPencil
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=> {
                                    setCurrentValues(content.find(key => key.id === item));
                                    isEditVisible(true);
                                }}
                            />
                        </Tooltip>
                        <Popconfirm
                            title="Вы уверены, что хотите удалить этого специалиста?"
                            onConfirm={()=>{deleteSpecialist(item)}}
                            okText="Удалить"
                            okType="danger primary"
                            cancelText="Отмена"
                            placement="leftTop"
                        >
                            <Tooltip title="Удалить">
                                <BsTrash
                                    className="text-white bg-red-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                    id={item}
                                    onClick={(record)=>(setCurrentSpecialist(record.target.id))}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                )
            }
        }
    ];

    return (
        <div>
            {token &&
            <AdminLayout>
                <PageHeader
                    title="Специалисты"
                    className="border-b-2 border-blue-600"
                    extra={[
                        <ImPlus
                            className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                            key={"1"}
                            onClick={()=>{isVisible(true)}}
                        />
                    ]}
                />

                <Modal
                    title="Новый специалист"
                    visible={visible}
                    onCancel={()=>{isVisible(false)}}
                    width={'80%'}
                    style={{height: "auto", top: 0, padding: 1}}
                    footer={null}
                >
                    <AddClinicRecord
                        recordType="specialist"
                        onTeamSuccess={()=>{
                            isVisible(false);
                            updateData();
                        }}
                    />
                </Modal>

                <Modal
                    title="Редактировать запись"
                    visible={editVisible}
                    onCancel={()=>{
                        isEditVisible(false);
                        setCurrentValues(null);
                    }}
                    width={'80%'}
                    style={{height: "auto", top: 10, padding: 1, overflow: 'hidden'}}
                    footer={null}
                >
                    {editVisible &&
                    <EditClinicRecord
                        initValues={currentValues}
                        onTeamSuccess={()=>{
                            isEditVisible(false);
                            updateData();
                        }}
                        fields={[
                            {name: ["fullName"], value: currentValues["fullName"]},
                            {name: ["preview"], value: currentValues.preview},
                            {name: ["equipmentIdList"], value: currentValues.equipmentIdList},
                            {name: ["drugIdList"], value: currentValues.drugIdList},
                            {name: ["clinicServiceIdList"], value: currentValues.clinicServiceCategoryId},
                        ]}
                        recordType="specialist"
                    />
                    }
                </Modal>

                <Drawer
                    placement="right"
                    onClose={()=>{
                        isDrawer(false);
                        setDrawerTeam(null);
                    }}
                    visible={drawer}
                    width="30%"
                >
                    <section className="font-open-sans mx-2 rounded-md shadow-md shadow-gray-300 md:mx-10 lg:mx-15 xl:mx-20">
                        {drawerTeam &&
                        <div className="w-120">
                            <img
                                src={drawerTeam.imagePath}
                                alt={drawerTeam.fullName}
                                className="h-auto w-full mx-auto rounded-t-md mb-4"
                            />
                            <Tag color="grey" className="m-1 rounded-md">{moment(drawerTeam.creationTime).format("DD.MM.YYYY   HH:mm")}</Tag>
                            <div className="p-6">
                                <h1 className="text-lg font-oswald font-bold md:text-xl lg:text-2xl xl:text-3xl">
                                    {drawerTeam.fullName}
                                </h1>
                                {drawerTeam.preview &&
                                <h3 className="text-md md:text-lg lg:text-xl xl:text-2xl">
                                    {drawerTeam.preview}
                                </h3>
                                }
                            </div>
                            <Collapse ghost onChange={()=>{isCollapse(!collapse)}}>
                                <Collapse.Panel header={collapse ? "Свернуть" : "Далее"}>
                                    <div
                                        dangerouslySetInnerHTML={{__html: drawerTeam.content}}
                                        className="text-base m-3 md:text-md lg:text-lg xl:text-xl"
                                    />
                                </Collapse.Panel>
                            </Collapse>
                        </div>
                        }
                    </section>
                </Drawer>

                {content &&
                <Table
                    columns={columns}
                    dataSource={content}
                    bordered
                    className="my-4"
                    size="small"
                    pagination={{position: ["bottomCenter"]}}
                />
                }
            </AdminLayout>
            }
        </div>
    )
}