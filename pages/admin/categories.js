import React, {useEffect, useState} from "react";
import {notification, PageHeader, Popconfirm, Table, Tag, Tooltip} from "antd";
import 'antd/dist/antd.css';
import {Modal} from "antd";
import AdminLayout from "../../components/AdminComponents/AdminLayout";
import {useRouter} from "next/router";
import API from "../../components/API/API";
import AddClinicRecord from "../../components/AdminComponents/AddClinicRecord";
import {ImPlus} from "react-icons/im";
import moment from "moment";
import {BsArrowsFullscreen, BsPencil, BsTrash} from "react-icons/bs";
import EditClinicRecord from "../../components/AdminComponents/EditClinicRecord";


export default function Category () {

    const [visible, isVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [data, setData] = useState(null);
    const [content, setContent] = useState(null);
    const [currentValues, setCurrentValues] = useState(null);
    const [currentCategory, setCurrentCategory] = useState(null);
    const [editVisible, isEditVisible] = useState(false);
    const [collapse, isCollapse] = useState(false);
    const router = useRouter();

    useEffect(()=>{
        if (!localStorage['token']) {
            router.push('/admin');
        } else {
            setToken(localStorage['token']);
        }
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (data === null) {
            updateData();
        }
    }, []);

    function updateData () {
        API.get('api/clinic-services/category')
            .then(function (response){
                setData(response.data.clinicServiceCategoryDataPage);
                setContent(response.data.clinicServiceCategoryDataPage.content);
            })
            .catch(function (error){
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message
                })
            })
    }

    function deleteCategory (id) {
        API.delete(`api/clinic-services/category/${id}`)
            .then((response)=>{
                notification['success']({
                    message: response.data.message,
                    className: "font-open-sans"
                })
                updateData();
            })
            .catch((error)=>{
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    const columns = [
        {
            title: "Наименование",
            dataIndex: "name",
            key: "1",
            width: "16%",
        },
        {
            title: "Канонический URL",
            dataIndex: "hiddenName",
            key: "10",
            width: "16%"
        },
        {
            title: "Дата публикации",
            dataIndex: "creationTime",
            key: "3",
            width: "15%",
            render: item => moment(item).format("DD.MM.YYYY | HH:mm")
        },
        {
            title: "Дата последнего редактирования",
            dataIndex: "updateTime",
            key: "4",
            width: "15%",
            render: item => moment(item).format("DD.MM.YYYY | HH:mm")
        },
        {
            title: "Главное изображение",
            dataIndex: "imagePath",
            key: "5",
            width: "10%",
            render: (item)=>{
                if (item) {
                    return <img key={item} alt="photo" src={item} className="max-h-14 w-auto mx-auto"/>
                } else {
                    return <Tag color="red">Отсутствует</Tag>
                }
            }
        },
        {
            title: "Действия",
            dataIndex: "id",
            key: "6",
            width: "10%",
            render: (item)=>{
                return (
                    <div className="flex flex-row justify-around font-open-sans" key={item}>
                        <Tooltip title="Редактировать">
                            <BsPencil
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=> {
                                    setCurrentValues(content.find(key => key.id === item));
                                    isEditVisible(true);
                                }}
                            />
                        </Tooltip>
                        <Popconfirm
                            title="Вы уверены, что хотите удалить эту категорию?"
                            onConfirm={()=>{deleteCategory(item)}}
                            okText="Удалить"
                            okType="danger primary"
                            cancelText="Отмена"
                            placement="leftTop"
                        >
                            <Tooltip title="Удалить">
                                <BsTrash
                                    className="text-white bg-red-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                    id={item}
                                    onClick={(record)=>(setCurrentCategory(record.target.id))}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                )
            }
        }
    ];

    return (
        <div>
            {token &&
            <AdminLayout>
                <PageHeader
                    title="Категории"
                    className="border-b-2 border-blue-600"
                    extra={[
                        <ImPlus
                            className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                            key={"1"}
                            onClick={()=>{isVisible(true)}}
                        />
                    ]}
                />

                <Modal
                    title="Новая категория"
                    visible={visible}
                    onCancel={()=>{isVisible(false)}}
                    width={'80%'}
                    style={{height: "auto", top: 0, padding: 1}}
                    footer={null}
                >
                    <AddClinicRecord
                        recordType="category"
                        onCategorySuccess={()=>{
                            isVisible(false);
                            updateData();
                        }}
                    />
                </Modal>

                <Modal
                    title="Редактировать запись"
                    visible={editVisible}
                    onCancel={()=>{
                        isEditVisible(false);
                        setCurrentValues(null);
                    }}
                    width={'80%'}
                    style={{height: "auto", top: 10, padding: 1, overflow: 'hidden'}}
                    footer={null}
                >
                    {editVisible &&
                    <EditClinicRecord
                        initValues={currentValues}
                        fields={[
                            {name: ["name"], value: currentValues["name"]},
                            {name: ["preview"], value: currentValues.preview},
                            {name: ["clinicServiceIdList"], value: currentValues.clinicServiceCategoryId},
                        ]}
                        onCategorySuccess={()=>{
                            isEditVisible(false);
                            updateData();
                        }}
                        recordType="category"
                    />
                    }
                </Modal>

                {content &&
                <Table
                    columns={columns}
                    dataSource={content}
                    bordered
                    className="my-4"
                    size="small"
                    pagination={{position: ["bottomCenter"]}}
                />
                }
            </AdminLayout>
            }
        </div>
    )
}