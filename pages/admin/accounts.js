import React, {useEffect, useState} from "react";
import {Button, Modal, Form, Input, notification, Table, PageHeader} from "antd";
import API from "../../components/API/API";
import AdminLayout from "../../components/AdminComponents/AdminLayout";
import {useRouter} from "next/router";
import {ImPlus} from "react-icons/im";

export default function Accounts () {

    const [visible, isVisible] = useState(false);
    const [token, setToken] = useState(false);
    const [userList, setList] = useState(null);
    const router = useRouter();

    useEffect(()=>{
        if (!localStorage['token']) {
            router.push('/admin');
        } else {
            setToken(localStorage['token']);
        }
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (!userList) {
            updateList();
        }
    }, []);

    const onFinish = (values) => {
        API.post('api/account', values)
            .then(function(response){
                isVisible(false);
                notification['success']({
                    message: 'Аккаунт успешно создан!',
                    description: 'Спасибо, что воспользовались нашим сервисом',
                    className: "font-open-sans"
                })
            })
            .catch(function(error){
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    };

    const updateList = () => {
        API.get('api/account')
        .then (function(response){
            setList(response.data.accountsDataPage.content);
        })
        .catch (function(error){
            console.dir(error)
        })
    }

    const columns = [
       {
        title: "Имя",
        dataIndex: "fullName",
        key: "1",
       },
       {
        title: "Логин",
        dataIndex: "login",
        key: "2",
       },
       {
        title: "Email",
        dataIndex: "email",
        key: "3",
       },
       {
        title: "ID",
        dataIndex: "id",
        key: "4",
       },
    ]

    return (
        <div>
            {token &&
            <AdminLayout>
                    <PageHeader
                        title="Пользователи"
                        className="border-b-2 border-blue-600"
                        extra={[
                            <ImPlus
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                key={"add"}
                                onClick={()=>{isVisible(true)}}
                            />
                        ]}
                    />
                    <Modal
                        visible={visible}
                        footer={null}
                        onCancel={()=>{isVisible(false)}}
                        title="Создание пользователя"
                    >
                        <Form layout="vertical" name="nest-messages" onFinish={onFinish}>
                            <Form.Item name={'login'} label="Логин" rules={[{ required: true, message: 'Не выбран логин' }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name={'email'} label="Email" rules={[{ type: 'email', required: true, message: 'Не выбран email' }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name={'firstName'} label="Имя" rules={[{ required: true, message: 'Не выбрано имя' }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name={'lastName'} label="Фамилия" rules={[{ required: true, message: 'Не выбрана фамилия' }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name={'password'} label="Пароль" rules={[{ required: true, message: 'Не выбран пароль' }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    Создать
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    <Table 
                        columns={columns}
                        dataSource={userList}
                        bordered
                        size="small"
                        className="my-4"
                        pagination={{position: ["bottomCenter"]}}
                    />
            </AdminLayout>
            }
        </div>
    )
}