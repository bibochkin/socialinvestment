import {notification} from "antd";
import React, {useEffect, useState} from 'react';
import 'antd/dist/antd.css';
import API from "../../components/API/API";
import jwt_decode from "jwt-decode";
import store from "../../store/store";
import {tokenData} from "../../store/tokenReducer";
import AuthForm from "../../components/AdminComponents/AuthForm";
import moment from "moment";
import axios from "axios";
import {useRouter} from "next/router";

function AdminPage () {

    const router = useRouter();
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        if (localStorage['token']) {
            if (moment().valueOf() > 1000*jwt_decode(localStorage['token']).exp) {               // - 86395000
                localStorage.removeItem('token');
            } else {
                API.defaults.headers.common['Authorization'] = localStorage['token'];
                router.push('/admin/news');
            }
        }
    }, [])

    function logIn (item) {
        let decoded = jwt_decode(item);
        store.dispatch(tokenData(decoded));
        router.push('/admin/promotions');
    }

    function onFinish (values) {
        setLoading(true)
        axios.post(`https://backsunrise.idea-tmn.ru/api/auth/login`, values)
            .then(function (response) {
                localStorage.setItem('token', response.data.token);
                logIn(response.data.token);
                API.defaults.headers.common['Authorization'] = response.data.token;
                setLoading(false)
            })
            .catch(function(error) {
                notification['error']({
                    message: 'Неверный логин или пароль',
                    description: error.message,
                    className: "font-open-sans"
                })
                setLoading(false)
            })
    }

    function onFinishFailed (errorInfo) {
        notification['error']({
            message: 'Пожалуйста, заполните обязательные поля',
            className: "font-open-sans"
        })
    }

    return (
        <div className="min-h-screen w-[100%] flex flex-col justify-center bg-gray-200 font-open-sans 2xl:px-[35%] xl:px-[30%] lg:px-[25%] md:px-[20%] sm:px-[10%]">
                <AuthForm onFinish={onFinish} onFinishFailed={onFinishFailed} loading={loading}/>
        </div>
    );
}

export default AdminPage
