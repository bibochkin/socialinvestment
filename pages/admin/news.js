import React, {useEffect, useState} from "react";
import 'antd/dist/antd.css';
import AddRecord from "../../components/AdminComponents/AddRecord";
import API from "../../components/API/API";
import {ImPlus} from "react-icons/im";
import {Modal, PageHeader, notification, Popconfirm, Tooltip, Table, Drawer, Collapse, Tag} from 'antd';
import {BsTrash, BsPencil, BsArrowsFullscreen} from 'react-icons/bs'
import moment from "moment";
import EditRecord from "../../components/AdminComponents/EditRecord";
import store from "../../store/store";
import {setNewsData} from "../../store/newsReducer";
import AdminLayout from "../../components/AdminComponents/AdminLayout";
import {useRouter} from "next/router";


export default function News () {

    const [visible, isVisible] = useState(false);
    const [newsList, setNewsList] = useState(null);
    const [currentNews, setCurrentNews] = useState(null);
    const [editVisible, isEditVisible] = useState(false);
    const [currentValues, setCurrentValues] = useState(null);
    const [drawer, isDrawer] = useState(false);
    const [drawerNews, setDrawerNews] = useState(null);
    const [collapse, isCollapse] = useState(false);
    const [token, setToken] = useState(null);
    const router = useRouter();
    
    useEffect(()=>{
        if (!localStorage['token']) {
            router.push('/admin');
        } else {
            setToken(localStorage['token']);
        }
        API.defaults.headers.common['Authorization'] = localStorage['token'];
        if (newsList === null) {
            updateData();
        }
    }, []);

    function updateData () {
        API.get('api/dynamicPages/news')
            .then(function (response) {
                store.dispatch(setNewsData(response.data.allNewsData));
                setNewsList(response.data.allNewsData);
            })
            .catch(function (error) {
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    function deleteNews (id) {
        API.delete(`api/dynamicPages/news/${id}`)
            .then((response)=>{
                notification['success']({
                    message: response.data.message,
                    className: "font-open-sans"
                })
                updateData();
            })
            .catch((error)=>{
                notification['error']({
                    message: 'Что-то пошло не так :(',
                    description: error.message,
                    className: "font-open-sans"
                })
            })
    }

    const columns = [
        {
            title: "Заголовок",
            dataIndex: "title",
            key: "1",
            width: "15%"
        },
        {
            title: "Канонический URL",
            dataIndex: "hiddenTitle",
            key: "10",
            width: "15%"
        },
        {
            title: "Краткое описание",
            dataIndex: "preview",
            key: "2",
            width: "15%"
        },
        {
            title: "Дата публикации",
            dataIndex: "creationTime",
            key: "3",
            width: "15%",
            render: item => moment(item).format("DD.MM.YYYY | HH:mm")
        },
        {
            title: "Дата последнего редактирования",
            dataIndex: "updateTime",
            key: "4",
            width: "15%",
            render: item => moment(item).format("DD.MM.YYYY | HH:mm")
        },
        {
            title: "Главное изображение",
            dataIndex: "imagePath",
            key: "5",
            width: "15%",
            render: (item)=>{
                return <img key={item} alt="photo" src={item} className="max-h-14 w-auto mx-auto"/>
            }
        },
        {
            title: "Действия",
            dataIndex: "id",
            key: "6",
            width: "10%",
            render: (item)=>{
                return (
                    <div className="flex flex-row justify-between font-open-sans" key={item}>
                        <Tooltip title="Просмотр">
                            <BsArrowsFullscreen
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=>{
                                    setDrawerNews(newsList.find(key => key.id === item));
                                    isDrawer(true);
                                }}
                            />
                        </Tooltip>
                        <Tooltip title="Редактировать">
                            <BsPencil
                                className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                id={item}
                                onClick={()=> {
                                    setCurrentValues(newsList.find(key => key.id === item));
                                    isEditVisible(true);
                                }}
                            />
                        </Tooltip>
                        <Popconfirm
                            title="Вы уверены, что хотите удалить эту новость?"
                            onConfirm={()=>{deleteNews(item)}}
                            okText="Удалить"
                            okType="danger primary"
                            cancelText="Отмена"
                            placement="leftTop"
                        >
                            <Tooltip title="Удалить">
                                <BsTrash
                                    className="text-white bg-red-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                                    id={item}
                                    onClick={(record)=>(setCurrentNews(record.target.id))}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                )
            }
        }
    ];

    return (
        <div>
            {token &&
            <AdminLayout>
                <PageHeader
                    title="Новости"
                    className="border-b-2 border-blue-600"
                    extra={[
                        <ImPlus
                            className="text-white bg-blue-600 text-2xl rounded p-2 m-1 h-8 w-8 hover:cursor-pointer"
                            key={"1"}
                            onClick={()=>{isVisible(true)}}
                        />
                    ]}
                />
                <Modal
                    title="Новая запись"
                    visible={visible}
                    onCancel={()=>{isVisible(false)}}
                    width={'80%'}
                    style={{height: "auto", top: 10, padding: 1, overflow: 'hidden'}}
                    footer={null}
                >
                    <AddRecord
                        onNewsSuccess={()=>{
                            setCurrentValues(null);
                            isVisible(false);
                            updateData();
                        }}
                        recordType="news"
                    />
                </Modal>
                <Modal
                    title="Редактировать новость"
                    visible={editVisible}
                    onCancel={()=>{
                        setCurrentValues(null);
                        isEditVisible(false);
                    }}
                    width={'80%'}
                    style={{height: "auto", top: 10, padding: 1, overflow: 'hidden'}}
                    footer={null}
                >
                    {editVisible &&
                    <EditRecord
                        initValues={currentValues}
                        fields={[
                            {name: ["title"], value: currentValues.title},
                            {name: ["preview"], value: currentValues.preview}
                        ]}
                        onNewsSuccess={()=>{
                            isEditVisible(false);
                            updateData();
                        }}
                        recordType="news"
                    />
                    }
                </Modal>
                <Drawer
                    placement="right"
                    onClose={()=>{
                        isDrawer(false);
                        setDrawerNews(null);
                    }}
                    visible={drawer}
                    width="50%"
                >
                    <section className="font-open-sans mx-2 rounded-md shadow-md shadow-gray-300 md:mx-10 lg:mx-15 xl:mx-20">
                        {drawerNews &&
                        <div className="w-120">
                            <img
                                src={drawerNews.imagePath}
                                alt={drawerNews.title}
                                className="h-auto w-full mx-auto rounded-lg shadow-lg mb-4"
                            />
                            <Tag color="grey" className="m-1 rounded-md">{moment(drawerNews.creationTime).format("DD.MM.YYYY   HH:mm")}</Tag>
                            <div className="p-6">
                                <h1 className="text-lg font-oswald font-bold md:text-xl lg:text-2xl xl:text-3xl">
                                    {drawerNews.title}
                                </h1>
                                {drawerNews.preview &&
                                <h3 className="text-md md:text-lg lg:text-xl xl:text-2xl">
                                    {drawerNews.preview}
                                </h3>
                                }
                            </div>
                            <Collapse ghost onChange={()=>{isCollapse(!collapse)}}>
                                <Collapse.Panel header={collapse ? "Свернуть" : "Далее"}>
                                    <div
                                        dangerouslySetInnerHTML={{__html: drawerNews.content}}
                                        className="text-lg m-3"
                                    />
                                </Collapse.Panel>
                            </Collapse>
                        </div>
                        }
                    </section>
                </Drawer>
                {newsList &&
                <Table
                    dataSource={newsList}
                    columns={columns}
                    bordered
                    className="my-4"
                    size="small"
                    pagination={{position: ["bottomCenter"]}}
                />
                }
            </AdminLayout>
            }
        </div>
    )
}